{% load humanize common %}

ขอบคุณสำหรับการบริจาค
เราได้รับเงินบริจาคประจำเดือน{{ sequence.due_date|date:"F Y" }} ของคุณเรียบร้อยแล้ว

ตัดเงินเมื่อวันที่ {{ transaction.created|date:"j N Y เวลา H:i น." }}

------------------------------

รายละเอียดการบริจาค

บริจาคให้โครงการ{{donation.campaign.name}}
ยอดบริจาค {{donation.amount | amount_short}} บาทต่อเดือน
เริ่มบริจาคเมื่อวันที่ {{donation.date_confirmed|date:"j N Y เวลา H:i น." }}
เลขที่การบริจาค #{{donation.donation_number}}

ชื่อผู้บริจาค
{{donation.donor.display_name}}

------------------------------

ข้อมูลการชำระเงิน

ชำระผ่าน{{payment_method}}
{% if extra_details %}
{{extra_details.card_name}}
{{extra_details.last_digits}}
{% endif %}

{% if donation.invoice_addressee and donation.invoice_address %}
ออกใบเสร็จรับเงินในนาม
{{donation.invoice_addressee}}
{{donation.invoice_address}}
{% endif %}

------------------------------

หากผู้บริจาคต้องการหยุดการบริจาคแบบรายเดือน หรือต้องการเปลี่ยนแปลงข้อมูลการบริจาค กรุณาส่งอีเมลแจ้งความต้องการมาที่ {{ settings.WEBSITE_SUPPORT_EMAIL }}

==============================

เว็บไซต์เทใจดอทคอม {{ settings.WEBSITE_URL }}