{% load humanize common %}
ขอบคุณสำหรับการบริจาค
เราได้ตรวจสอบการโอนเงินบริจาคของคุณเรียบร้อยแล้ว

------------------------------

รายละเอียดการบริจาค

บริจาคให้โครงการ{{donation.campaign.name}}
ยอดบริจาค {{donation.amount | amount_short}} บาท
บริจาคเมื่อวันที่ {{ donation.date_confirmed|date:"j N Y เวลา H:i น." }}
เลขที่การบริจาค #{{donation.donation_number}}

ชื่อผู้บริจาค
{{donation.donor.display_name}}

------------------------------

ข้อมูลการชำระเงิน
ชำระผ่านบัญชีธนาคาร

{% if donation.invoice_addressee and donation.invoice_address %}
ออกใบเสร็จรับเงินในนาม
{{donation.invoice_addressee}}
{{donation.invoice_address}}
{% endif %}

==============================

เว็บไซต์เทใจดอทคอม {{ settings.WEBSITE_URL }}
