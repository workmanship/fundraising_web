# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404

from fundraising.modules.store_commerce.functions import get_functions as commerce_fn
from fundraising.modules.store_commerce.models import Product, Cart, Order

from .forms import CheckoutForm


def listing(request):
    products = Product.objects.published()

    return render(request, 'pages/listing.html', {
        'products': products,
    })


def product_view(request, product_id):
    product = get_object_or_404(Product, pk=product_id)

    if not product.is_published:
        raise Http404

    return render(request, 'pages/product_page.html', {
        'product': product,
    })


def cart_view(request):
    cart = Cart.objects.get_from_request(request)
    cart.update()

    return render(request, 'pages/cart.html', {

    })


def checkout(request):
    cart = Cart.objects.get_from_request(request)

    if not cart or cart.is_empty:
        return redirect('commerce:cart_view')

    if request.method == 'POST':
        checkout_form = CheckoutForm(request.POST)

        if checkout_form.is_valid():
            checkout_form.save(cart)
            return redirect('commerce:checkout_payment')

    else:
        initial = {}
        if cart.shopper:
            shopper = cart.shopper
            initial.update({
                'first_name': shopper.first_name,
                'last_name': shopper.last_name,
                'email': shopper.email,
            })

        if cart.shipping_address:
            shipping_address = cart.shipping_address
            initial.update({
                'phone_number': shipping_address.phone_number,
                'address': shipping_address.address,
                'city': shipping_address.city,
                'province': shipping_address.province,
                'zip_code': shipping_address.zip_code,
            })

        checkout_form = CheckoutForm(initial=initial)

    return render(request, 'commerce/checkout.html', {
        'checkout_form': checkout_form,
    })


def checkout_payment(request):
    cart = Cart.objects.get_from_request(request)

    if request.method == 'POST':
        order = commerce_fn.checkout(cart)

        if order.total == 0:
            order.set_no_payment_required()

        else:

            # DUMMY : Assume payment is collected right away
            order.set_payment_paid()

            # request.session[''] = ''  # TODO : Add success message after purchase

        order.save()

        commerce_fn.unlink_cart(request)

        # TODO : Clear cart

        return redirect('commerce:order_details', order_number=order.order_number)

    return render(request, 'commerce/checkout_payment.html', {})


def order_details(request, order_number):
    order = Order.objects.get(order_number=order_number)

    return render(request, 'commerce/order_details.html', {
        'order': order,
    })
