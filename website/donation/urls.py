# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^listing/$', view=views.listing, name='listing'),
    url(r'^product/(?P<product_id>\d+)/$', view=views.product_view, name='product_view'),

    url(r'^cart/$', view=views.cart_view, name='cart_view'),

    url(r'^checkout/$', view=views.checkout, name='checkout'),
    url(r'^checkout/payment/$', view=views.checkout_payment, name='checkout_payment'),

    url(r'^order/(?P<order_number>\w+)/$', view=views.order_details, name='order_details'),

]
