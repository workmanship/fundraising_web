# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.core import validators

from fundraising.constants import TH_PROVINCE_CHOICES
from fundraising.modules.store_commerce.models import Cart

"""
class AddToCartForm(forms.Form):
    quantity = forms.IntegerField(validators=[validators.MinValueValidator(1)])

    def save(self, request, product):
        cart = Cart.objects.get_or_create_from_request(request)
        quantity = self.cleaned_data['quantity']
        return cart.add_product_to_cart(product, quantity)
"""


class CheckoutForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    # Address
    address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    city = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    province = forms.ChoiceField(choices=TH_PROVINCE_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    zip_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def save(self, cart):
        cart.update_shopper(
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            email=self.cleaned_data['email']
        )

        fullname = '{} {}'.format(self.cleaned_data['first_name'], self.cleaned_data['last_name'])

        cart.update_shipping_address(
            fullname=fullname,
            address=self.cleaned_data['address'],
            city=self.cleaned_data['city'],
            province=self.cleaned_data['province'],
            country='TH',
            zip_code=self.cleaned_data['zip_code'],
            phone_number=self.cleaned_data['phone_number'],
        )

        cart.update_billing_address(
            fullname=fullname,
            address=self.cleaned_data['address'],
            city=self.cleaned_data['city'],
            province=self.cleaned_data['province'],
            country='TH',
            zip_code=self.cleaned_data['zip_code'],
            phone_number=self.cleaned_data['phone_number'],
        )

        return cart


class CheckoutPaymentForm(forms.Form):
    pass
