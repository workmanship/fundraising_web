# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from smtplib import SMTPException

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.urls import reverse

from celery.exceptions import MaxRetriesExceededError

from website.celery import app


@app.task(bind=True)
def send_recurring_donation_started_email(self, donation, instruction, transaction=None):
    payment_method = u'ระบบ'
    extra_details = {}
    if transaction:
        if transaction.payment_provider == 'omise':
            payment_method = u'บัตรเครดิต'
            extra_details = {
                'card_name': transaction.customer_card.card.card_name,
                'last_digits': transaction.customer_card.card.get_card_last_digits_display(),
            }
        elif transaction.payment_provider == 'linepay':
            payment_method = u'LINE Pay'

    email_context = {
        'settings': settings,
        'donation': donation,
        'instruction': instruction,
        'payment_method': payment_method,
        'extra_details': extra_details,
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [donation.donor.email]

    subject = render_to_string('donation/emails/donation_confirmed_subject.txt', email_context)
    message = render_to_string('donation/emails/donation_confirmed.txt', email_context)
    html_message = render_to_string('donation/emails/donation_confirmed.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass


@app.task(bind=True)
def send_recurring_donation_paid_email(self, sequence, transaction):
    payment_method = u'ระบบ'
    extra_details = {}
    if transaction.payment_provider == 'omise':
        payment_method = u'บัตรเครดิต'
        extra_details = {
            'card_name': transaction.customer_card.card.card_name,
            'last_digits': transaction.customer_card.card.get_card_last_digits_display(),
        }
    elif transaction.payment_provider == 'linepay':
        payment_method = u'LINE Pay'

    email_context = {
        'settings': settings,
        'donation': sequence.instruction.donation,
        'sequence': sequence,
        'transaction': transaction,
        'payment_method': payment_method,
        'extra_details': extra_details,
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [sequence.instruction.donation.donor.email]

    subject = render_to_string('donation/emails/recurring_donation_paid_subject.txt', email_context)
    message = render_to_string('donation/emails/recurring_donation_paid.txt', email_context)
    html_message = render_to_string('donation/emails/recurring_donation_paid.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass


@app.task(bind=True)
def send_remind_card_expiration_email(self, donation):
    donation_url = settings.WEBSITE_URL
    if donation.campaign:
        donation_url += reverse('donation:change_card', args=[donation.donation_number])

    extra_details = {}
    if donation.recurring_instruction.payment_provider == 'omise':
        card = donation.recurring_instruction.instruction.omise.customer_card.card
        extra_details = {
            'card_name': card.card_name,
            'card_last_digits': card.get_card_last_digits_display(),
            'card_expiration_month_year': card.card_expiration_month_year(),
        }

    email_context = {
        'settings': settings,
        'donation': donation,
        'donation_url': donation_url,
        'extra_details': extra_details,
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [donation.donor.email]

    subject = render_to_string('donation/emails/remind_card_expiration_subject.txt', email_context)
    message = render_to_string('donation/emails/remind_card_expiration.txt', email_context)
    html_message = render_to_string('donation/emails/remind_card_expiration.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass


@app.task(bind=True)
def send_remind_card_expiring_email(self, donation):
    donation_url = settings.WEBSITE_URL
    if donation.campaign:
        donation_url += reverse('donation:change_card', args=[donation.donation_number])

    extra_details = {}
    if donation.recurring_instruction.payment_provider == 'omise':
        card = donation.recurring_instruction.instruction.omise.customer_card.card
        extra_details = {
            'card_name': card.card_name,
            'card_last_digits': card.get_card_last_digits_display(),
            'card_expiration_month_year': card.card_expiration_month_year(),
        }

    email_context = {
        'settings': settings,
        'donation': donation,
        'donation_url': donation_url,
        'extra_details': extra_details,
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [donation.donor.email]

    subject = render_to_string('donation/emails/remind_card_expiring_subject.txt', email_context)
    message = render_to_string('donation/emails/remind_card_expiring.txt', email_context)
    html_message = render_to_string('donation/emails/remind_card_expiring.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass


@app.task(bind=True)
def send_success_donation_paid_email(self, donation, transaction):
    if donation.donor.email:
        payment_method = u'ระบบ'
        extra_details = {}
        if transaction.payment_provider == 'omise':
            payment_method = u'บัตรเครดิต'
            extra_details = {
                'card_name': transaction.customer_card.card.card_name,
                'last_digits': transaction.customer_card.card.get_card_last_digits_display(),
            }
        elif transaction.payment_provider == 'linepay':
            payment_method = u'LINE Pay'

        email_context = {
            'settings': settings,
            'donation': donation,
            'payment_method': payment_method,
            'extra_details': extra_details,
        }

        from_email = settings.WEBSITE_FROM_EMAIL
        recipient_list = [donation.donor.email]

        subject = render_to_string('donation/emails/donation_paid_subject.txt', email_context)
        message = render_to_string('donation/emails/donation_paid.txt', email_context)
        html_message = render_to_string('donation/emails/donation_paid.html', email_context)

        try:
            send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
        except SMTPException as e:
            try:
                raise self.retry(exc=e)
            except MaxRetriesExceededError:
                # TODO : Log as critical
                pass


@app.task(bind=True)
def send_waiting_donation_transfer_email(self, donation, transaction):
    if donation.donor.email:
        email_context = {
            'settings': settings,
            'donation': donation,
            'confirmation_url': '%s%s' % (
                settings.WEBSITE_URL, reverse('donation:thankyou', args=[donation.donation_number]))
        }

        from_email = settings.WEBSITE_FROM_EMAIL
        recipient_list = [donation.donor.email]

        subject = render_to_string('donation/emails/donation_transfer_waiting_subject.txt', email_context)
        message = render_to_string('donation/emails/donation_transfer_waiting.txt', email_context)
        html_message = render_to_string('donation/emails/donation_transfer_waiting.html', email_context)

        try:
            send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
        except SMTPException as e:
            try:
                raise self.retry(exc=e)
            except MaxRetriesExceededError:
                # TODO : Log as critical
                pass


@app.task(bind=True)
def send_accept_donation_transfer_email(self, donation, confirmation=None):
    email_context = {
        'settings': settings,
        'donation': donation,
        'confirmation': confirmation,
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [donation.donor.email]

    subject = render_to_string('donation/emails/donation_transfer_accept_subject.txt', email_context)
    message = render_to_string('donation/emails/donation_transfer_accept.txt', email_context)
    html_message = render_to_string('donation/emails/donation_transfer_accept.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass



@app.task(bind=True)
def send_reject_donation_transfer_email(self, donation, confirmation=None):
    email_context = {
        'settings': settings,
        'donation': donation,
        'confirmation': confirmation,
        'confirmation_url': '%s%s' % (
            settings.WEBSITE_URL, reverse('donation:thankyou', args=[donation.donation_number]))
    }

    from_email = settings.WEBSITE_FROM_EMAIL
    recipient_list = [donation.donor.email]

    subject = render_to_string('donation/emails/donation_transfer_reject_subject.txt', email_context)
    message = render_to_string('donation/emails/donation_transfer_reject.txt', email_context)
    html_message = render_to_string('donation/emails/donation_transfer_reject.html', email_context)

    try:
        send_mail(subject, message, from_email, recipient_list, fail_silently=False, html_message=html_message)
    except SMTPException as e:
        try:
            raise self.retry(exc=e)
        except MaxRetriesExceededError:
            # TODO : Log as critical
            pass
