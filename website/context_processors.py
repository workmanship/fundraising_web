from django.conf import settings


def project_settings(request):
    return {
        'settings': settings,
        'UPLOAD_SETTINGS': settings.UPLOAD_SETTINGS,
        'CURRENT_URL': '{}{}'.format(settings.WEBSITE_URL, request.path),
    }
