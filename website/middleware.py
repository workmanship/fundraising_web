# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#from fundraising.modules.store_commerce.constants import CART_SESSION_ID
#from fundraising.modules.store_commerce.models import Cart, Shopper


class ForceDefaultLanguageMiddleware(object):
    """
    Ignore Accept-Language HTTP headers

    This will force the I18N machinery to always choose settings.LANGUAGE_CODE
    as the default initial language, unless another one is set via sessions or cookies

    Should be installed *before* any middleware that checks request.META['HTTP_ACCEPT_LANGUAGE'],
    namely django.middleware.locale.LocaleMiddleware
    """

    def process_request(self, request):
        if 'HTTP_ACCEPT_LANGUAGE' in request.META.keys():
            del request.META['HTTP_ACCEPT_LANGUAGE']

"""
class ShoppingCartMiddleware(object):

    def process_request(self, request):
        cart_id = request.session.get(CART_SESSION_ID, None)

        if not cart_id:
            cart = Cart()
        else:
            try:
                cart = Cart.objects.get(id=cart_id)
                cart.update()
            except Cart.DoesNotExist:
                cart = Cart()
                del request.session[CART_SESSION_ID]

        # Prevent `save() prohibited to prevent data loss due to unsaved related object`
        # if not cart.shopper:
        #     cart.shopper = Shopper()

        request.shopping_cart = cart
"""
