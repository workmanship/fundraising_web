# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import timedelta
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

import backyard
from backyard.pages import BackyardPage
from backyard.pages.listview import ListViewSearchMixin
from backyard.utils import extract_date_range_for_query
from fundraising.modules.fund_donation.enums import DonationPaymentStatus

from fundraising.modules.fund_donation.models import Donation

from website.backyard.forms.donations import ConfirmaedDonationsSearchForm, IncompleteDonationsSearchForm, \
    AllDonationsSearchForm


class ConfirmedDonationsPage(BackyardPage, ListViewSearchMixin, ListView):
    title = _('Donations')
    urlpattern = r'^donations/$'
    template_name = 'backyard/donations/donations_confirmed.html'

    model = Donation
    queryset = Donation.objects.confirmed_donations()
    ordering = ('-date_submitted',)
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(ConfirmedDonationsPage, self).get_context_data(**kwargs)
        return context

    def get_form_class(self):
        return ConfirmaedDonationsSearchForm

    def get_queryset(self):
        queryset = super(ConfirmedDonationsPage, self).get_queryset()
        search_form = self.search_form

        # Search criteria

        if search_form.cleaned_data.get('keyword'):
            keyword = search_form.cleaned_data.get('keyword')
            queryset = queryset.filter(Q(donor_first_name__icontains=keyword) |
                                       Q(donor_last_name__icontains=keyword) |
                                       Q(donor_email__icontains=keyword) |
                                       Q(donor_phone_number__icontains=keyword) |
                                       Q(receipt_address__icontains=keyword))

        if search_form.cleaned_data.get('date_submitted_range'):
            from_date, to_date = extract_date_range_for_query(search_form.cleaned_data.get('date_submitted_range'))
            to_date = to_date + timedelta(days=+1)  # To include last date in the result
            queryset = queryset.filter(date_submitted__range=[from_date, to_date])

        return queryset

backyard.register_url(ConfirmedDonationsPage)


class IncompleteDonationsPage(BackyardPage, ListViewSearchMixin, ListView):
    title = _('Donations')
    urlpattern = r'^donations/failed/$'
    template_name = 'backyard/donations/donations_incomplete.html'

    model = Donation
    ordering = ('-date_submitted',)
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(IncompleteDonationsPage, self).get_context_data(**kwargs)
        return context

    def get_form_class(self):
        return IncompleteDonationsSearchForm

    def get_queryset(self):
        queryset = super(IncompleteDonationsPage, self).get_queryset()
        search_form = self.search_form

        if search_form.cleaned_data.get('keyword'):
            keyword = search_form.cleaned_data.get('keyword')
            queryset = queryset.filter(Q(donor_first_name__icontains=keyword) |
                                       Q(donor_last_name__icontains=keyword) |
                                       Q(donor_email__icontains=keyword) |
                                       Q(donor_phone_number__icontains=keyword) |
                                       Q(receipt_address__icontains=keyword))

        if search_form.cleaned_data.get('date_submitted_range'):
            from_date, to_date = extract_date_range_for_query(search_form.cleaned_data.get('date_submitted_range'))
            to_date = to_date + timedelta(days=+1)  # To include last date in the result
            queryset = queryset.filter(date_submitted__range=[from_date, to_date])

        query_status = search_form.cleaned_data.get('payment_status')
        allowed_status = [
            DonationPaymentStatus.OFFLINE_PAYMENT_WAITING,
            DonationPaymentStatus.OFFLINE_PAYMENT_DECLINED,
            DonationPaymentStatus.PAYMENT_FAILED,
            DonationPaymentStatus.RECURRING_STOPPED_BY_STAFF,
            DonationPaymentStatus.RECURRING_STOPPED_BY_SYSTEM,
            DonationPaymentStatus.RECURRING_STOPPED_BY_USER]

        if query_status:
            queryset = queryset.filter(
                payment_status__in=[status for status in query_status if status in allowed_status])
        else:
            queryset = queryset.filter(
                payment_status__in=allowed_status)

        return queryset

backyard.register_url(IncompleteDonationsPage)


class AllDonationsPage(BackyardPage, ListViewSearchMixin, ListView):
    title = _('Donations')
    urlpattern = r'^donations/all/$'
    template_name = 'backyard/donations/donations_all.html'

    model = Donation
    ordering = ('-date_submitted',)
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(AllDonationsPage, self).get_context_data(**kwargs)
        return context

    def get_form_class(self):
        return AllDonationsSearchForm

    def get_queryset(self):
        queryset = super(AllDonationsPage, self).get_queryset()
        search_form = self.search_form

        # Search criteria

        if search_form.cleaned_data.get('keyword'):
            keyword = search_form.cleaned_data.get('keyword')
            queryset = queryset.filter(Q(donor_first_name__icontains=keyword) |
                                       Q(donor_last_name__icontains=keyword) |
                                       Q(donor_email__icontains=keyword) |
                                       Q(donor_phone_number__icontains=keyword) |
                                       Q(receipt_address__icontains=keyword))

        if search_form.cleaned_data.get('date_submitted_range'):
            from_date, to_date = extract_date_range_for_query(search_form.cleaned_data.get('date_submitted_range'))
            to_date = to_date + timedelta(days=+1)  # To include last date in the result
            queryset = queryset.filter(date_submitted__range=[from_date, to_date])

        if search_form.cleaned_data.get('payment_status'):
            queryset = queryset.filter(payment_status__in=search_form.cleaned_data.get('payment_status'))

        return queryset

backyard.register_url(AllDonationsPage)


class DonationPage(BackyardPage, DetailView):
    title = _('Donation Details')
    urlpattern = r'^donations/(?P<number>[\w-]+)/$'
    template_name = 'backyard/donations/donation.html'
    model = Donation

    def get_object(self, **kwargs):
        return get_object_or_404(Donation, donation_number=self.kwargs['number'])

    def get_context_data(self, **kwargs):
        context = super(DonationPage, self).get_context_data(**kwargs)

        transactions = []
        context['transactions'] = transactions
        # context['total_amount'] = recurring.sequences.filter(paid_transaction__isnull=False).aggregate(total=Sum('paid_transaction__amount'))['total']

        return context

backyard.register_url(DonationPage)


class DonorsPage(BackyardPage, TemplateView):
    title = _('Donors')
    urlpattern = r'^donors/$'
    template_name = 'backyard/donations/donors.html'

backyard.register_url(DonorsPage)


class RecurringPage(BackyardPage, TemplateView):
    title = _('Recurring')
    urlpattern = r'^recurring/$'
    template_name = 'backyard/donations/recurring.html'

backyard.register_url(RecurringPage)


class CampaignsPage(BackyardPage, TemplateView):
    title = _('Campaigns')
    urlpattern = r'^campaigns/$'
    template_name = 'backyard/donations/campaigns.html'

backyard.register_url(CampaignsPage)
