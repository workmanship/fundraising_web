# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Sum, Count
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

import backyard
from backyard.pages import BackyardPage
from fundraising.modules.fund_donation.models import DonationCampaign, Donation


class DashboardPage(BackyardPage, TemplateView):
    title = _('Dashboard')
    urlpattern = r'^$'
    template_name = 'backyard/pages/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardPage, self).get_context_data(**kwargs)
        #
        # context['sum_donated'] = \
        #     Donation.objects.all_valid_donations().aggregate(total=Sum('cumulative_amount'))['total'] or 0
        #
        # context['num_one_off_donors'] = \
        #     Donation.objects.all_valid_recurring_donations().aggregate(donor_count=Count('donor'))['donor_count'] or 0
        #
        # context['num_recurring_donors'] = \
        #     Donation.objects.all_valid_one_off_donations().aggregate(donor_count=Count('donor'))['donor_count'] or 0
        #
        # context['num_donors'] = context['num_one_off_donors'] + context['num_recurring_donors']
        #
        # context['num_campaigns'] = DonationCampaign.objects.ongoing_campaigns().count()

        return context

backyard.register_url(DashboardPage)


class StatisticsPage(BackyardPage, TemplateView):
    title = _('Statistics')
    urlpattern = r'^stats/$'
    template_name = 'backyard/pages/statistics.html'

backyard.register_url(StatisticsPage)


class OptimizationPage(BackyardPage, TemplateView):
    title = _('Optimization')
    urlpattern = r'^optimization/$'
    template_name = 'backyard/pages/optimization.html'

backyard.register_url(OptimizationPage)

