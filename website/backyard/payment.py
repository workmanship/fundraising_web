# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

import backyard
from backyard.pages import BackyardPage


class BankTransferPage(BackyardPage, TemplateView):
    title = _('Bank Transfer')
    urlpattern = r'^bank_transfer/$'
    template_name = 'backyard/payment/bank_transfer.html'

backyard.register_url(BankTransferPage)


class TransactionsPage(BackyardPage, TemplateView):
    title = _('Transaction')
    urlpattern = r'^transactions/$'
    template_name = 'backyard/payment/transactions.html'

backyard.register_url(TransactionsPage)
