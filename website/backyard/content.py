# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

import backyard
from backyard.pages import BackyardPage


class HomepagePage(BackyardPage, TemplateView):
    title = _('Homepage')
    urlpattern = r'^homepage/$'
    template_name = 'backyard/content/homepage.html'

backyard.register_url(HomepagePage)


class SiteContentPage(BackyardPage, TemplateView):
    title = _('Site Content')
    urlpattern = r'^site_content/$'
    template_name = 'backyard/content/site_content.html'

backyard.register_url(SiteContentPage)


class EmailManagerPage(BackyardPage, TemplateView):
    title = _('Email Manager')
    urlpattern = r'^email_manager/$'
    template_name = 'backyard/content/email_manager.html'

backyard.register_url(EmailManagerPage)
