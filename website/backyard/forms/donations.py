# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from fundraising.modules.fund_donation.enums import DonationPaymentStatus


class ConfirmaedDonationsSearchForm(forms.Form):
    keyword = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'e.g. Number, Name, Email'}), required=False)
    date_submitted_range = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))


class IncompleteDonationsSearchForm(forms.Form):
    PAYMENT_STATUS_CHOICES = (
        (DonationPaymentStatus.OFFLINE_PAYMENT_WAITING, DonationPaymentStatus.OFFLINE_PAYMENT_WAITING_TEXT),
        (DonationPaymentStatus.OFFLINE_PAYMENT_DECLINED, DonationPaymentStatus.OFFLINE_PAYMENT_DECLINED_TEXT),
        (DonationPaymentStatus.PAYMENT_FAILED, DonationPaymentStatus.PAYMENT_FAILED_TEXT),
        (DonationPaymentStatus.RECURRING_STOPPED_BY_STAFF, DonationPaymentStatus.RECURRING_STOPPED_BY_STAFF_TEXT),
        (DonationPaymentStatus.RECURRING_STOPPED_BY_SYSTEM, DonationPaymentStatus.RECURRING_STOPPED_BY_SYSTEM_TEXT),
        (DonationPaymentStatus.RECURRING_STOPPED_BY_USER, DonationPaymentStatus.RECURRING_STOPPED_BY_USER_TEXT),
    )

    keyword = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'e.g. Number, Name, Email'}), required=False)
    date_submitted_range = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    payment_status = forms.MultipleChoiceField(choices=PAYMENT_STATUS_CHOICES, widget=forms.SelectMultiple(attrs={'class': 'form-control'}), required=False)


class AllDonationsSearchForm(forms.Form):
    keyword = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'e.g. Number, Name, Email'}), required=False)
    date_submitted_range = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    payment_status = forms.MultipleChoiceField(choices=DonationPaymentStatus.DATA_CHOICES, widget=forms.SelectMultiple(attrs={'class': 'form-control'}), required=False)
