# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

import backyard
from backyard.pages import BackyardPage


class ReportsPage(BackyardPage, TemplateView):
    title = _('Reports')
    urlpattern = r'^reports/$'
    template_name = 'backyard/report/reports.html'

backyard.register_url(ReportsPage)


class ActionLogsPage(BackyardPage, TemplateView):
    title = _('Action Logs')
    urlpattern = r'^homepage/$'
    template_name = 'backyard/report/action_logs.html'

backyard.register_url(ActionLogsPage)
