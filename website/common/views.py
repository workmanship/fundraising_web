# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.middleware import csrf
from django.views.decorators.cache import never_cache


@never_cache
def get_form_csrf(request):
    token = csrf.get_token(request)

    return HttpResponse(token)
