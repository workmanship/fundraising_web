# -*- coding: utf-8 -*-
import re
from decimal import Decimal

from django import template
from django.conf import settings
from django.template import defaultfilters

from easy_thumbnails.files import get_thumbnailer
import pytz

register = template.Library()


@register.assignment_tag
def define(val=None):
    return val


@register.simple_tag
def get_settings(name):
    return getattr(settings, name, '')


@register.filter
def thumbnail(obj, alias):
    if obj:
        return get_thumbnailer(obj)[alias].url
    else:
        field_name = obj.field.__str__()
        empty_thumbnail_settings = settings.EMPTY_THUMBNAIL_ALIASES.get(field_name)

        if empty_thumbnail_settings:
            try:
                thumbnail_settings = settings.THUMBNAIL_ALIASES[field_name][alias]

                thumbnail_file = open('%s/%s' % (settings.APPS_DIR.path('static'), empty_thumbnail_settings['filepath']))
                thumbnailer = get_thumbnailer(thumbnail_file, relative_name=empty_thumbnail_settings['filepath'])
                return thumbnailer.get_thumbnail({
                    'crop': thumbnail_settings['crop'],
                    'size': thumbnail_settings['size'],
                }).url

            except:
                # TODO : Add to logs
                pass

        return ''


@register.filter
def datetime_short(datetime):
    if datetime:
        bangkok_timezone = pytz.timezone('Asia/Bangkok')
        datetime = bangkok_timezone.normalize(datetime)
        year_BE = str(datetime.year + 543)
        display_date = defaultfilters.date(datetime, (u'd M {year} เวลา H:i น.').format(year=year_BE[-2:]))
    else:
        display_date = '-'
    return display_date


@register.filter
def amount_short(amount):
    if amount is None:
        return '-'

    try:
        return '{0:,.0f}'.format(amount)
    except ValueError:
        return '-'


@register.filter
def removetags(html, tags):
    tags = [re.escape(tag) for tag in tags.split()]
    tags_re = '(%s)' % '|'.join(tags)
    starttag_re = re.compile(r'<%s(/?>|(\s+[^>]*>))' % tags_re, re.U)
    endtag_re = re.compile('</%s>' % tags_re)
    html = starttag_re.sub('', html)
    html = endtag_re.sub('', html)
    return html


@register.assignment_tag
def first_or_equal_choice(is_first, choice_value, form_value, form_custom_value):
    if form_value:
        try:
            return Decimal(choice_value) == Decimal(form_value)
        except:
            return False

    else:
        if form_custom_value:
            return False

        return is_first
