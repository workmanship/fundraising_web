# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Run celery with celery beats'

    def handle(self, *args, **options):
        os.system("celery -A website worker -l info -B")
