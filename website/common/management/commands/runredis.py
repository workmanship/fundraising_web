# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Run redis'

    def handle(self, *args, **options):
        os.system("redis-server")
