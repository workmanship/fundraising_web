# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from fundraising.enums import TransactionActionStatus
from fundraising.modules.fund_donation.enums import DonationPaymentStatus
from fundraising.payments.exceptions import PaymentException
from fundraising.payments.omise.exceptions import OMISE_ERROR_CODES
from regular.forms import RegularForm

from fundraising.modules.fund_donation import functions as donation_fn

from fundraising.payments.providers import is_valid_payment_option, extract_payment_option


class PaymentFormError(Exception):
    def __init__(self, message, field=None):
        self.field = field
        super(PaymentFormError, self).__init__(message)


class PaymentForm(RegularForm):
    payment_option = forms.CharField(widget=forms.HiddenInput())

    def clean_payment_option(self):
        payment_option = self.cleaned_data.get('payment_option')

        if not is_valid_payment_option(payment_option):
            raise forms.ValidationError(_('Invalid payment option'))

        return payment_option

    def save_payment(self, request, donation):
        provider_code, payment_method = extract_payment_option(self.cleaned_data['payment_option'])

        donation.payment_provider = provider_code
        donation.payment_method = payment_method
        donation.set_payment_status_processing()
        donation.save()

        provider = donation.get_payment_provider()

        if provider_code == 'omise':
            if payment_method == 'creditcard':
                omise_token = request.POST.get('omise_token')
                omise_card_id = request.POST.get('omise_card_id')

                if omise_token and omise_card_id:
                    try:
                        omise_customer_card = provider.create_customer_with_card(donation.person, omise_token,
                                                                                 omise_card_id)
                    except PaymentException as e:
                        raise PaymentFormError(e.message)

                    else:
                        if not donation.is_recurring:
                            # One-time donation
                            charge_transaction = provider.charge_card(donation, omise_customer_card)

                            if charge_transaction.action_status == TransactionActionStatus.SUCCESS:
                                donation.set_payment_status_paid(charge_transaction)
                                donation.save()

                            else:
                                donation.set_payment_status_failed(charge_transaction)
                                donation.save()

                        else:
                            # Recurring donation
                            instruction = donation.recurring_instruction
                            provider.initialize_recurring(instruction, omise_customer_card)

                            # Capturing the first charge
                            sequence = instruction.get_or_create_first_sequence()
                            charge_transaction = donation_fn.charge_recurring_donation(sequence)

                            if charge_transaction.action_status == TransactionActionStatus.SUCCESS:
                                donation_fn.start_recurring(donation, charge_transaction)
                            else:
                                donation.set_payment_status_failed(charge_transaction)
                                donation.save()

                        if donation.payment_status in (DonationPaymentStatus.PAID, DonationPaymentStatus.RECURRING):
                            # SUCCESS!!!
                            return donation

                        else:
                            error_msg = OMISE_ERROR_CODES.get(charge_transaction.fail_code,
                                                              _('Cannot process this credit card. '
                                                                'Please try again or change to '
                                                                'another card.'))

                            raise PaymentFormError(error_msg)

                else:
                    raise PaymentFormError(_('Cannot process this credit card. '
                                             'Please try again or change to '
                                             'another card.'))

            elif payment_method == 'ibanking':
                pass

        elif provider_code == 'linepay':
            pass

        elif provider_code == 'transfer':
            pass

        elif provider_code == 'giftcard':
            pass

        return donation


class DonationForm(PaymentForm):
    amount_choice = forms.ChoiceField()
    custom_amount = forms.DecimalField(required=False,
                                       widget=forms.NumberInput(attrs={'class': 'form-control'}))
    recurring = forms.BooleanField(required=False)
    first_name = forms.CharField(label=_('First name'), max_length=200,
                                 widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label=_('Last name'), max_length=200,
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label=_('Email'), max_length=100, required=False,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
    phone_number = forms.CharField(label=_('Phone number'), max_length=100, required=False,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(label=_('Address'), max_length=500, required=False,
                              widget=forms.Textarea(attrs={'class': 'form-control'}))

    def __init__(self, campaign, data=None, *args, **kwargs):
        super(DonationForm, self).__init__(data, *args, **kwargs)
        self.campaign = campaign

        data = data or {}

        amount_choices = donation_fn.get_donation_amount_choices(campaign)
        self.fields['amount_choice'] = forms.ChoiceField(
            required=False, choices=[(choice.amount, choice.description) for choice in amount_choices],
            initial=data.get('amount_choice')
        )

    def clean_amount_choice(self):
        amount_choice = self.cleaned_data['amount_choice']

        if amount_choice:
            try:
                amount_choice = Decimal(amount_choice)
            except:
                raise forms.ValidationError('Invalid donation amount value')

            if amount_choice < settings.MINIMUM_DONATION_AMOUNT:
                raise forms.ValidationError(
                    'Donation amount must not less than {}'.format(settings.MINIMUM_DONATION_AMOUNT))

            if amount_choice > settings.MAXIMUM_DONATION_AMOUNT:
                raise forms.ValidationError(
                    'Donation amount must not greater than {}'.format(settings.MAXIMUM_DONATION_AMOUNT))

        return amount_choice

    def clean_custom_amount(self):
        custom_amount = self.cleaned_data['custom_amount']

        if custom_amount:
            try:
                custom_amount = Decimal(custom_amount)
            except:
                raise forms.ValidationError('Invalid donation amount value')

            if custom_amount < settings.MINIMUM_DONATION_AMOUNT:
                raise forms.ValidationError(
                    'Donation amount must not less than {}'.format(settings.MINIMUM_DONATION_AMOUNT))

            if custom_amount > settings.MAXIMUM_DONATION_AMOUNT:
                raise forms.ValidationError(
                    'Donation amount must not greater than {}'.format(settings.MAXIMUM_DONATION_AMOUNT))

        return custom_amount

    def clean(self):
        cleaned_data = super(DonationForm, self).clean()
        amount_choice = cleaned_data.get('amount_choice')
        custom_amount = cleaned_data.get('custom_amount')

        if not amount_choice and not custom_amount:
            # If amount_choice or custom_amount already thrown errors, will not add more
            if not self._errors.get('amount_choice') and not self._errors.get('custom_amount'):
                self.add_error('amount_choice', 'Please specify donation amount')

    def save(self, request):
        amount = self.cleaned_data['amount_choice']
        if not amount:
            amount = self.cleaned_data['custom_amount']

        first_name = self.cleaned_data['first_name']
        last_name = self.cleaned_data['last_name']
        email = self.cleaned_data['email']
        phone_number = self.cleaned_data['phone_number']
        receipt_address = self.cleaned_data['address']

        recurring = self.cleaned_data['recurring']

        if recurring:
            recurring_interval = settings.DEFAULT_RECURRING_INTERVAL
        else:
            recurring_interval = None

        person = donation_fn.get_or_update_donor(request, first_name, last_name, email, phone_number, receipt_address)
        donation = donation_fn.new_donation(amount, person, self.campaign, recurring_interval)

        try:
            self.save_payment(request, donation)
        except PaymentFormError as e:
            raise e

        return donation
