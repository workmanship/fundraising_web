# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from fundraising.modules.fund_donation.enums import DonationPaymentStatus
from regular.utils.numbers import display_amount

register = template.Library()


@register.filter
def payment_status_text(payment_status):
    status_dict = DonationPaymentStatus.DATA_MAP.get(payment_status)
    return status_dict['text'] if status_dict else 'Unknown'


# Display functions
# ----------------------------------------------------------------------------------------------------------------------

@register.simple_tag
def display_donation_amount(donation):
    if donation.is_recurring:
        recurring = donation.recurring_interval__display
        recurring_str = ' <span class="recurring"><i class="fa fa-refresh"></i> {recurring}</span>'.format(
            recurring=recurring)
    else:
        recurring_str = ' <span class="recurring">One-time</span>'

    return mark_safe('<span class="number">{amount} Baht</span>{recurring_str}'.format(
        amount=display_amount(donation.amount), recurring_str=recurring_str))


@register.simple_tag
def display_donation_campaign(donation):
    if donation.campaign:
        return donation.campaign.name
    else:
        return 'No campaign'


@register.simple_tag
def display_donation_payment(donation):
    status_dict = DonationPaymentStatus.DATA_MAP.get(donation.payment_status)

    if status_dict:
        status_text = status_dict['text']
        status_looking = status_dict['looking']
    else:
        status_text = 'Unknown'
        status_looking = 'notgood'

    method = donation.payment_method__display

    return mark_safe('<span class="text {looking}">{status_text}</span> <span class="method">{method}</span>'.format(
        looking=status_looking, status_text=status_text, method=method))


@register.simple_tag
def display_donation_fail_reason(donation):
    reason = 'N/A'

    if donation.payment_status == DonationPaymentStatus.OFFLINE_PAYMENT_WAITING:
        reason = _('Waiting for donor to submit proof of payment')

    elif donation.payment_status == DonationPaymentStatus.OFFLINE_PAYMENT_DECLINED:
        reason = _('Proof of payment is declined by admin')

    elif donation.payment_status == DonationPaymentStatus.PAYMENT_FAILED:
        reason = _('System failed to collect payment')

    elif donation.payment_status == DonationPaymentStatus.RECURRING_STOPPED_BY_STAFF:
        reason = _('Recurring donation was stopped by admin')

    elif donation.payment_status == DonationPaymentStatus.RECURRING_STOPPED_BY_SYSTEM:
        reason = _('Recurring donation was stopped due to multiple failed payment')

    elif donation.payment_status == DonationPaymentStatus.RECURRING_STOPPED_BY_USER:
        reason = _('Recurring donation was stopped by donor')

    return reason
