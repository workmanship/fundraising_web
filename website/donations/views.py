# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext_lazy as _

from fundraising.models import Person
from fundraising.modules.fund_donation.models import DonationCampaign, Donation
from fundraising.payments.exceptions import PaymentException
from fundraising.payments.omise.exceptions import OMISE_ERROR_CODES

from website.donations.forms import DonationForm, PaymentFormError


def _donate_view(request, campaign=None):
    if request.method == 'POST':
        if campaign and not campaign.is_able_to_donate:
            # TODO : Add django message saying 'This campaign is closed for donation'
            return redirect(campaign.get_absolute_url())

        post_data = request.POST.dict()
        if request.user.is_authenticated:
            post_data.update({
                'email': request.user.email,
            })

        form = DonationForm(campaign=campaign, data=post_data)
        if form.is_valid():

            try:
                donation = form.save(request)
                return redirect('donations:thankyou', donation_number=donation.donation_number)

            except PaymentFormError as e:
                if e.field:
                    form.add_field_error(e.field, e.message)
                else:
                    form.add_non_field_error(e.message)

    else:
        initial_data = {}

        if request.user.is_authenticated():
            user = request.user

            try:
                person = Person.objects.get(user=user)
            except Person.DoesNotExist:
                person = Person()

            initial_data.update({
                'first_name': person.first_name if person.first_name else user.first_name,
                'last_name': person.last_name if person.last_name else user.last_name,
                'email': person.email if person.email else user.email,
                'phone_number': person.phone_number,
                'address': person.receipt_address,
            })

        form = DonationForm(campaign=campaign, initial=initial_data)

    return render(request, 'donations/donate.html', {
        'campaign': campaign,
        'form': form,
    })


def donate(request):
    return _donate_view(request, None)


def donate_by_campaign(request, campaign_id, campaign_slug):
    if campaign_id:
        campaign = get_object_or_404(DonationCampaign, pk=campaign_id)
    elif campaign_slug:
        campaign = get_object_or_404(DonationCampaign, slug=campaign_slug)
    else:
        raise Http404

    if not campaign.is_public:
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise Http404

    return _donate_view(request, campaign)


"""
    if request.method == 'POST':
        post_data = request.POST.dict()
        if request.user.is_authenticated:
            post_data.update({
                'email': request.user.email,
            })

        form = DonationForm(campaign=None, data=post_data)
        if form.is_valid():

            try:
                donation = form.save(request)
                return redirect('donations:thankyou', donation_number=donation.donation_number)

            except PaymentFormError as e:
                if e.field:
                    form.add_field_error(e.field, e.message)
                else:
                    form.add_non_field_error(e.message)

    else:
        initial_data = {
            'amount_choice': 1000,
            'payment_option': '',
        }

        if request.user.is_authenticated():
            user = request.user

            try:
                person = Person.objects.get(user=user)
            except Person.DoesNotExist:
                person = Person()

            initial_data.update({
                'first_name': person.first_name if person.first_name else user.first_name,
                'last_name': person.last_name if person.last_name else user.last_name,
                'email': person.email if person.email else user.email,
                'phone_number': person.phone_number,
                'address': person.receipt_address,
            })

        form = DonationForm(campaign=None, initial=initial_data)

    return render(request, 'donations/donate.html', {
        'form': form,
    })
"""

"""
    if request.method == 'POST':
        if not campaign.is_able_to_donate:
            # TODO : Add django message saying 'This campaign is closed for donation'
            return redirect(campaign.get_absolute_url())

        post_data = request.POST.dict()
        if request.user.is_authenticated:
            post_data.update({
                'email': request.user.email,
            })

        form = SimpleDonationForm(request, post_data)

        if form.is_valid():
            donation = form.save(request, campaign)

            if donation.payment_provider == 'omise':
                provider = donation.payment_provider_object

                omise_token = request.POST.get('omise_token')
                omise_card_id = request.POST.get('omise_card_id')

                if omise_token and omise_card_id:
                    try:
                        omise_customer_card = provider.create_customer_with_card(donation.donor, omise_token,
                                                                                 omise_card_id)

                    except PaymentException as e:
                        form.add_form_error(e.message)

                    else:
                        omise_transaction = provider.charge_card(donation, omise_customer_card)

                        if omise_transaction.action_status == StoreTransactionStatus.SUCCESS:
                            donation.set_to_paid(omise_transaction)
                        else:
                            donation.set_to_failed(omise_transaction)
                            form.add_form_error(omise_transaction.fail_message)

                        if donation.status in (DonationStatus.PAID, DonationStatus.RECURRING):
                            return redirect('donations:thankyou', donation_number=donation.donation_number)

                        else:
                            try:
                                error_msg = OMISE_ERROR_CODES[omise_transaction.fail_code]
                            except KeyError:
                                error_msg = \
                                    _('Cannot process this credit card. Please try again or change to another card.')

                            form.add_form_error(omise_transaction.fail_message)

                else:
                    form.add_form_error('Missing token')

            elif donation.payment_provider == 'linepay':
                pass

            elif donation.payment_provider == 'transfer':
                pass

            else:
                raise Http404

            return redirect('donations:thankyou', donation.donation_number)

    else:
        if request.user.is_authenticated():
            user = request.user

            try:
                person = Person.objects.get(user=user)
            except Person.DoesNotExist:
                person = Person()

            initial_data = {
                'first_name': person.first_name if person.first_name else user.first_name,
                'last_name': person.last_name if person.last_name else user.last_name,
                'email': person.email if person.email else user.email,
                'phone_number': person.phone_number,
                'address': person.receipt_address,
            }

        else:
            initial_data = {}

        form = SimpleDonationForm(request, initial=initial_data)

    return render(request, 'donations/campaign.html', {
        'campaign': campaign,
        'form': form,
    })
"""

"""
def donate_by_campaign(request, campaign_id, campaign_slug):

    if campaign_id:
        campaign = get_object_or_404(DonationCampaign, pk=campaign_id)
    elif campaign_slug:
        campaign = get_object_or_404(DonationCampaign, slug=campaign_slug)
    else:
        raise Http404

    if not campaign.is_visible_to_user:
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise Http404

    if request.method == 'POST':
        if not campaign.is_able_to_donate:
            # TODO : Add django message saying 'This campaign is closed for donation'
            return redirect(campaign.get_absolute_url())

        post_data = request.POST.dict()
        if request.user.is_authenticated:
            post_data.update({
                'email': request.user.email,
            })

        form = DonationForm(post_data)

        if form.is_valid():
            donation = form.save(request, campaign)
            payment_option = form.cleaned_data.get('payment_option')

            if payment_option == 'creditcard':
                provider = get_payment_provider('omise')

                omise_token = request.POST.get('omise_token')
                omise_card_id = request.POST.get('omise_card_id')

                if omise_token and omise_card_id:
                    try:
                        omise_customer_card = provider.create_customer_with_card(donation.donor, omise_token,
                                                                                 omise_card_id)

                    except PaymentException as e:
                        form.add_form_error(e.message)

                    else:
                        if donation.is_recurring:
                            # Initial charge on the day of donation submitted
                            omise_transaction = provider.charge_card(donation, omise_customer_card)

                            if omise_transaction.action_status == StoreTransactionStatus.SUCCESS:
                                provider.initialize_recurring(donation.recurring_instruction, omise_customer_card)
                                donation_fn.start_recurring(donation, omise_transaction)
                            else:
                                donation.set_to_failed(omise_transaction)
                                form.add_form_error(omise_transaction.fail_message)

                        else:
                            omise_transaction = provider.charge_card(donation, omise_customer_card)

                            if omise_transaction.action_status == StoreTransactionStatus.SUCCESS:
                                donation.set_to_paid(omise_transaction)
                            else:
                                donation.set_to_failed(omise_transaction)
                                form.add_form_error(omise_transaction.fail_message)

                        if donation.status in (DonationStatus.PAID, DonationStatus.RECURRING):
                            return redirect('donations:thankyou', donation_number=donation.donation_number)

                        else:
                            try:
                                error_msg = OMISE_ERROR_CODES[omise_transaction.fail_code]
                            except KeyError:
                                error_msg = \
                                    _('Cannot process this credit card. Please try again or change to another card.')

                            form.add_form_error(omise_transaction.fail_message)

                else:
                    form.add_form_error('Missing token')

            elif payment_option == 'linepay':
                pass

            elif payment_option == 'transfer':
                pass

            else:
                form.add_form_error('Invalid payment option')

    else:
        form = DonationForm(initial={
            'email': request.user.email if request.user.is_authenticated else '',
            'payment_option': 'creditcard',
            'real_amount': settings.DEFAULT_PREDEFINED_DONATION_AMOUNT,
            'real_recurring': '1m',
        })

    template_name = campaign.get_template_name()

    # TODO : Javascript to check if form has error, then scroll down to form

    return render(request, 'donation/donate_campaign.html', {
        'campaign': campaign,
        'form': form,
        'predefined': settings.PREDEFINED_DONATION_AMOUNT,
        'default_amount': settings.DEFAULT_PREDEFINED_DONATION_AMOUNT,
    })
"""


def thankyou(request, donation_number):
    donation = get_object_or_404(Donation, donation_number=donation_number)

    return render(request, 'donations/thankyou.html', {'donation': donation})
