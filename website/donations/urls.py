# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from . import views


urlpatterns = [


    url(r'^donate/id/(?P<campaign_id>\d+)/$', views.donate_by_campaign, {'campaign_slug': ''}, name='donate_campaign'),
    url(r'^donate/(?P<campaign_slug>[\w.+-]+)/$', views.donate_by_campaign, {'campaign_id': ''},
        name='donate_campaign_with_slug'),

    url(r'^donate/$', views.donate, name='donate'),

    url(r'^thankyou/(?P<donation_number>[\w.+-]+)/$', view=views.thankyou, name='thankyou'),
]
