"""
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SamplePagesModuleConfig(AppConfig):
    name = 'website.pages'
    verbose_name = "UNDP Pages"

    def ready(self):
        pass


default_app_config = 'website.pages.SamplePagesModuleConfig'
"""