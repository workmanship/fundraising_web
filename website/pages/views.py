# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from fundraising.modules.fund_donation.models import DonationCampaign


def homepage(request):
    campaigns = DonationCampaign.objects.ongoing_campaigns()

    return render(request, 'pages/homepage.html', {
        'campaigns': campaigns,
    })
