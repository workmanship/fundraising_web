# -*- coding: utf-8 -*-
from __future__ import unicode_literals


backyard_menu = {
    '': [
        ('home', 'website.backyard.pages.DashboardPage', 'fa-desktop'),
        ('link', 'website.backyard.pages.StatisticsPage', 'fa-pie-chart'),
        ('link', 'website.backyard.pages.OptimizationPage', 'fa-bar-chart'),

        ('section', 'DONATIONS'),
        ('link', 'website.backyard.donations.ConfirmedDonationsPage', 'fa-list'),
        ('link', 'website.backyard.donations.DonorsPage', 'fa-user-circle-o'),
        ('link', 'website.backyard.donations.RecurringPage', 'fa-refresh'),
        ('link', 'website.backyard.donations.CampaignsPage', 'fa-bullhorn'),

        ('section', 'CONTENT'),
        ('link', 'website.backyard.content.HomepagePage', 'fa-home'),
        ('link', 'website.backyard.content.SiteContentPage', 'fa-database'),
        ('link', 'website.backyard.content.EmailManagerPage', 'fa-envelope'),

        ('section', 'PAYMENT'),
        ('link', 'website.backyard.payment.BankTransferPage', 'fa-bank'),
        ('link', 'website.backyard.payment.TransactionsPage', 'fa-user-circle-o'),

        ('section', 'REPORT'),
        ('link', 'website.backyard.report.ReportsPage', 'fa-book'),
        ('link', 'website.backyard.report.ActionLogsPage', 'fa-history'),
    ],
}
