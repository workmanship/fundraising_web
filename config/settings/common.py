# -*- coding: utf-8 -*-
"""
Django settings for fundraising project.

Django settings documentation
https://docs.djangoproject.com/en/dev/topics/settings/

Django settings reference
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from __future__ import absolute_import, unicode_literals

import environ

ROOT_DIR = environ.Path(__file__) - 3  # (/a/b/myfile.py - 3 = /)
APPS_DIR = ROOT_DIR.path('website')

env = environ.Env()

WEBSITE_NAME = 'Sweeping Trekker'
WEBSITE_DOMAIN = 'workmanship.fund'  # Do not include subdomain
WEBSITE_URL = 'http://www.' + WEBSITE_DOMAIN

WEBSITE_EMAIL = 'admin@workmanship.fund'
WEBSITE_FROM_EMAIL = '{name} <{email}>'.format(name=WEBSITE_NAME, email=WEBSITE_EMAIL)

WEBSITE_SUPPORT_EMAIL = 'support@workmanship.fund'


# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Admin
    'django.contrib.admin',
)

THIRD_PARTY_APPS = (
    'compressor',
    'crispy_forms',
    'easy_thumbnails',
    'sequences.apps.SequencesConfig',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'website',
    'website.common',
    'website.pages',
    'website.donations',

    # Your stuff: custom apps go here

    'regular',

    'fundraising',
    'fundraising.payments.omise',
    'fundraising.payments.linepay',
    #'fundraising.payments.transfer',
    'fundraising.modules.fund_donation',
    'backyard',

)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'website.middleware.ForceDefaultLanguageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)


# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'website.contrib.sites.migrations'
}


# DEBUG
# ------------------------------------------------------------------------------
DEBUG = False


# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)


# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
ACCOUNT_EMAIL_SUBJECT_PREFIX = ''
EMAIL_SUBJECT_PREFIX = ''


# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
ADMINS = (
    ("""Support""", 'support@workmanship.fund'),
)

MANAGERS = ADMINS


# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
#TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Bangkok'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                # Your stuff: custom template context fundraisings go here
                'fundraising.context_processors.fundraising',
                'backyard.context_processors.backyard',
                'website.context_processors.project_settings',
            ],
        },
    },
]


# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------

STATIC_ROOT = str(ROOT_DIR('staticfiles'))
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    str(APPS_DIR.path('static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',

    'compressor.finders.CompressorFinder',
)

COMPRESS_OFFLINE_CONTEXT = {
    'STATIC_URL': STATIC_URL,
}


# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------

MEDIA_ROOT = str(APPS_DIR('media'))
MEDIA_URL = '/media/'


# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

ADMIN_URL = r'^admin/'


# CRISPY FORM
# ------------------------------------------------------------------------------

CRISPY_TEMPLATE_PACK = 'bootstrap3'

BOOTSTRAP3 = {
    'set_placeholder': False,
}


# UPLOAD
# ------------------------------------------------------------------------------

UPLOAD_SETTINGS = {
    'user_profile': {
        'max_size_in_mb': 5,
        'max_size_in_bytes': 1024 * 1024 * 5,
        'accepted_file': 'image/*'
    }
}


# EASY THUMBNAILS
# ------------------------------------------------------------------------------

THUMBNAIL_SAVE_ORIGINAL = {
    'user_profile': {'size': (1024, 1024)}
}

THUMBNAIL_ALIASES = {
    '': {
        'user_profile_uploader_thumbnail': {'size': (120, 120), 'crop': True},

    },
}

EMPTY_THUMBNAIL_ALIASES = {

}


# CELERY
# ------------------------------------------------------------------------------

CELERY_IMPORTS = ('website.donations.handlers', )  # Need to include all signal handlers here

CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = {'pickle'}


# REDIS
# ------------------------------------------------------------------------------
REDIS_SERVER = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 4


# STOREHOUSE
# ------------------------------------------------------------------------------

DONATION_RECURRING_CHARGE_DATE = 5  # Day 5th every month
DONATION_RECURRING_CHARGE_TIME = 8  # 8:00 AM

OMISE_RECURRING_FUND_TRANSFER_DATE = 25
OMISE_MONTHS_TO_REMIND_CARD_EXPIRED = 3

MINIMUM_DONATION_AMOUNT = 100
MAXIMUM_DONATION_AMOUNT = 1000000


# DONATION
# ------------

# DEFAULT_RECURRING_INTERVAL = '1m'  # 1 month
DEFAULT_RECURRING_INTERVAL = '3t'  # 3 minutes



# BACKYARD
# ------------------------------------------------------------------------------

DATE_INPUT_FORMATS = ('%d/%m/%Y', )
DATE_FORMAT = 'j M Y'

BACKYARD_TITLE_HOVER_COLOR = '#FEED01'
BACKYARD_MENU = 'config.backyard_menu'
BACKYARD_PAGES = (
    'website.backyard.pages',
    'website.backyard.donations',
    'website.backyard.content',
    'website.backyard.payment',
    'website.backyard.report',
)


# Your common stuff: Below this line define 3rd party library settings

PREDEFINED_DONATION_AMOUNT = (2000, 1000, 500)  # In baht
DEFAULT_PREDEFINED_DONATION_AMOUNT = 2000


TEMP_PROFILE_IMAGE_DIR = 'users/temp/avatar'
TEMP_PROFILE_IMAGE_FILE_TYPE = 'jpg'
