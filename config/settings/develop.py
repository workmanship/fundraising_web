# -*- coding: utf-8 -*-

from datetime import timedelta
from .common import *

from celery.schedules import crontab


DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

WEBSITE_DOMAIN = 'localhost:8000'

ALLOWED_HOSTS = ['localhost', ]

SECRET_KEY = 'THIS_IS_DUMMY_SECRET_KEY_FOR_DEVELOPMENT_ONLY'


# Database settings
# ------------------------------------------------------------------------------

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fundraising',
        'USER': 'fundraising',
        'PASSWORD': 'fundraising',
        'HOST': '',
        'PORT': '',
    },
}


# Mail settings
# ------------------------------------------------------------------------------
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Caching
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}


# Django Debug Toolbar
# ------------------------------------------------------------------------------
#MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
#INSTALLED_APPS += ('debug_toolbar', )

INTERNAL_IPS = ('127.0.0.1', )

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}


# Testing
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'


# Facebook App
# ------------------------------------------------------------------------------
FACEBOOK_APP_ID = '225585191119533'
FACEBOOK_SECRET_KEY = '0be20b2acb2df9dd3d869834a429821b'


# CELERY
# ------------------------------------------------------------------------------
BROKER_URL = 'redis://localhost:6379/2'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/2'

CELERYBEAT_SCHEDULE = {
    'generate-recurring-sequences': {
        'task': 'fundraising.modules.fund_donation.tasks.generate_sequences_from_instruction',
        'schedule': crontab()  # Every minutes
    },

    'charge-recurring-donation': {
        'task': 'fundraising.modules.fund_donation.tasks.charge_recurring_donation',
        'schedule': crontab()  # Every minutes
    },

    #'remind-omise-card-expiration': {
    #    'task': 'storehouse.payments.omise.tasks.remind_card_expiration',
    #    'schedule': crontab(minute='3,9,15,21,27,33,39,45,51,57')  # Every 6 minutes means 1 month
    #}
}

# if 'OMISE_RECURRING_FUND_TRANSFER_DATE' in locals() and OMISE_RECURRING_FUND_TRANSFER_DATE:
#     CELERYBEAT_SCHEDULE['transfer-omise-fund-monthly'] = {
#         'task': 'storehouse.payments.omise.tasks.transfer_fund_monthly',
#         'schedule': crontab(minute='4,10,16,22,28,34,40,46,52,58')  # Every 6 minutes means 1 month
#     }

# CELERY_ROUTES = {
#     'website.pages.tasks.send_recurring_donation_started_email': {'queue': 'email'}
# }


# CUSTOM CONFIGURATION
# ------------------------------------------------------------------------------

OMISE_SECRET_KEY = 'skey_test_4xgl105wtgkthpm8eni'
OMISE_PUBLIC_KEY = 'pkey_test_4xgl105wviy3i6jg6gn'


# ------------------------------------------------------------------------------

try:
    from .develop_local import *
except ImportError:
    pass
