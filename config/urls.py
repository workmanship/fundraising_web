# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views

import backyard

from website.views import bad_request, server_error

urlpatterns = [
    # Languages
    url(r'^i18n/', include('django.conf.urls.i18n')),

    # Django Admin
    url(settings.ADMIN_URL, include(admin.site.urls)),

    url(r'^backend/', include(backyard.get_urls(), namespace='backyard')),
    url(r'', include('fundraising.urls', namespace='fundraising')),

    # url(r'', include('fundraising.modules.store_commerce.urls', namespace='store_commerce')),

    url(r'', include('website.common.urls', namespace='common')),
    url(r'', include('website.pages.urls', namespace='pages')),
    url(r'', include('website.donations.urls', namespace='donations')),

    url(r'^summernote/', include('django_summernote.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler400 = 'website.views.bad_request'
handler500 = 'website.views.server_error'

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', bad_request),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', server_error),
    ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
