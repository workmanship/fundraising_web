# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import decimal
import importlib
import re
from datetime import datetime, date

import xlwt


# Class
# ----------------------------------------------------------------------------------------------------------------------
from django.utils.timezone import make_aware


def load_class_from_string(full_class_string):
    """
    dynamically load a class from a string
    """
    class_data = full_class_string.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]
    module = importlib.import_module(module_path)
    # Finally, we retrieve the Class
    return getattr(module, class_str)


# Date & Time
# ----------------------------------------------------------------------------------------------------------------------

def extract_date_range_for_query(daterange):
    print(daterange)
    from_date_str, to_date_str = daterange.split(' - ')

    from_date = make_aware(datetime.strptime(from_date_str, '%d/%m/%Y'))
    to_date = make_aware(datetime.strptime(to_date_str, '%d/%m/%Y'))

    return from_date, to_date


def convert_camel_case_to_string(x):
    return reduce(lambda a,b: a + ((b.upper() == b and (len(a) and a[-1].upper() != a[-1])) and (' ' + b) or b), x, '')


def url_name_from_class(cls, app_name=''):
    prefix = ('%s_' % app_name) if app_name else ''
    return '%s%s' % (prefix, re.sub('(?!^)([A-Z]+)', r'_\1', cls.__name__).lower())


def _autowidth(worksheet, column_no, label):
    if (type(label) == int) or (type(label) == decimal.Decimal):
        label = str(label)
    width = int((1+len(label)) * 256)
    if width > worksheet.col(column_no).width:
        worksheet.col(column_no).width = width


def render_to_xls(filename, sheetname, header_merge_x, header, result, hilight_columns=None):
    hilight_columns = hilight_columns or []

    wb = xlwt.Workbook()
    ws = wb.add_sheet(sheetname)

    fnt = xlwt.Font()
    fnt.bold = True

    al = xlwt.Alignment()
    al.horz = xlwt.Alignment.HORZ_CENTER

    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    pattern.pattern_fore_colour = xlwt.Style.colour_map['gray25']

    header_style = xlwt.XFStyle()
    header_style.font = fnt
    header_style.borders = borders
    header_style.pattern = pattern
    header_style.alignment = al

    hilight_pattern = xlwt.Pattern()
    hilight_pattern.pattern = xlwt.Pattern.SOLID_PATTERN
    hilight_pattern.pattern_fore_colour = xlwt.Style.colour_map['light_yellow']

    hilight_style = xlwt.XFStyle()
    hilight_style.pattern = hilight_pattern

    for i, row in enumerate(header_merge_x):
        current_y = 0
        for col in row:
            _autowidth(ws, current_y, col[1])
            ws.write_merge(i, i, current_y, current_y + col[0] - 1, col[1], header_style)
            current_y += col[0]


    header_length = 0
    if header:
        header_length = len(header)
        for i, row in enumerate(header):
            for j, col in enumerate(row):
                ws.write(i+len(header_merge_x), j, col, header_style)
                _autowidth(ws, j, col)


    for i, row in enumerate(result):
        for j, col in enumerate(row):
            if j in hilight_columns:
                ws.write(i+len(header_merge_x)+header_length, j, col, hilight_style)
            else:
                ws.write(i+len(header_merge_x)+header_length, j, col)
            _autowidth(ws, j, col)


    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename

    wb.save(response)
    return response
