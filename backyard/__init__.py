import importlib

from backyard.app_settings import BACKYARD_PAGES

_backyard_urls = []


def register_urls():
    for pages in BACKYARD_PAGES:
        importlib.import_module(pages)


def register_url(backyard_page_cls):
    urlpattern = backyard_page_cls.get_url()
    _backyard_urls.append(urlpattern) if urlpattern else None


def get_urls():
    return _backyard_urls


default_app_config = 'backyard.apps.BackyardConfig'
