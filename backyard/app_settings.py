from django.conf import settings


BACKYARD_SITE_NAME = getattr(settings, 'BACKYARD_SITE_NAME', settings.WEBSITE_NAME)

BACKYARD_TITLE_HOVER_COLOR = getattr(settings, 'BACKYARD_TITLE_HOVER_COLOR', '#74a0b7')

BACKYARD_MENU = getattr(settings, 'BACKYARD_MENU', '')
BACKYARD_PAGES = getattr(settings, 'BACKYARD_PAGES', '')
