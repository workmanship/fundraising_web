# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import urllib
from datetime import date

from django import template
from django.templatetags.tz import do_timezone
from django.utils import timezone
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def page_range_limit(page_range, page_number):
    page_number = int(page_number)
    return list(page_range)[max(0, page_number-5):page_number+4]


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = dict(context['request'].GET)
    query.update(kwargs)
    return urllib.urlencode(query, True)


@register.simple_tag
def querystring_to_hidden_field(query_dict, excludes=None):
    if isinstance(excludes, dict):
        excludes = excludes.keys()

    html_string = ''
    for query_name, query_value in query_dict.items():
        if query_name not in excludes:
            html_string = html_string.join('<input type="hidden" name="{name}" value="{value}" />'
                                           .format(name=query_name, value=query_value))

    return mark_safe(html_string)


@register.simple_tag
def querystring_include(query_dict, name=None, value=None):
    mutable_query_dict = query_dict.copy()

    if name:
        if value:
            mutable_query_dict[name] = value
        else:
            mutable_query_dict.pop(name, None)

    if len(mutable_query_dict):
        return '?{}'.format(mutable_query_dict.urlencode(True))
    else:
        return ''


@register.simple_tag
def querystring_only(query_dict, name):
    mutable_query_dict = query_dict.copy()

    value = mutable_query_dict.get(name, '')
    mutable_query_dict.clear()

    if value:
        mutable_query_dict[name] = value
        return '?{}'.format(mutable_query_dict.urlencode(True))

    else:
        return ''


@register.simple_tag
def display_datetime(datetime):
    if not datetime:
        return ''

    local_datetime = do_timezone(datetime, timezone.get_current_timezone())

    date_string = local_datetime.strftime('%d %b %Y')
    time_string = local_datetime.strftime('%I:%M %p')

    from django.utils.translation import get_language

    if get_language() == 'th':
        return '{} น.'.format(time_string)

    return mark_safe('<span class="date">{date}</span> <span class="time">{time}</span>'.format(
        date=date_string, time=time_string))


@register.simple_tag
def display_name_email(name, email):

    if not email:
        email = 'No email'
        email_classes = 'email no'
    else:
        email = '&lt;{}&gt;'.format(email)
        email_classes = 'email'

    return mark_safe('<span class="name">{name}</span> <span class="{email_classes}">{email}</span>'.format(
        name=name, email_classes=email_classes, email=email))


@register.filter
def display_amount(amount, with_html=True):
    if amount is None:
        return '0'

    try:
        float(amount)
    except ValueError:
        return '0'

    if with_html:

        precision_str = str(amount-int(amount))[1:]

        unit_html = ''
        if amount % 1:
            unit_html = '<span class="byd-amount-unit">{}</span>'.format(precision_str)

        try:
            return mark_safe('{:0,d}{}'.format(int(amount), unit_html))
        except ValueError:
            return '0'

    else:
        try:
            return mark_safe('{:0,.2f}'.format(amount))
        except ValueError:
            return '0'
