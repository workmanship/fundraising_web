# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms


#class ListViewSearchForm(forms.Form):
#    from_date = forms.DateField(required=False, input_formats=['%d/%m/%Y'], widget=forms.DateInput(format='%d/%m/%Y', attrs={'class': 'form-control'}))
#    to_date = forms.DateField(required=False, input_formats=['%d/%m/%Y'], widget=forms.DateInput(format='%d/%m/%Y', attrs={'class': 'form-control'}))
#    keyword = forms.CharField(max_length=200, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))


class ListViewSearchForm(forms.Form):
    keyword = forms.CharField(max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
