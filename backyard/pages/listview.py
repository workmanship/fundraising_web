# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import operator
from datetime import date

from django.db.models import Q
from django.views.generic import FormView
from django.views.generic.edit import FormMixin

from backyard.forms import ListViewSearchForm


class ListViewSearchMixin(FormMixin):
    search_form = None
    search_form_values = None

    def get_form_class(self):
        raise NotImplementedError

    def get_search_form(self):
        return self.search_form

    def get_search_form_values(self):
        return self.search_form_values

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = form_class(self.request.GET)
        form.is_valid()

        form_values = {}
        for field in form.fields:
            field_value = form.cleaned_data.get(field)

            if field_value and field_value is not None:
                form_values[field] = field_value

        form.is_empty = len(form_values) == 0

        self.search_form = form
        self.search_form_values = form_values

        return super(ListViewSearchMixin, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListViewSearchMixin, self).get_context_data(**kwargs)
        context['search_form'] = self.search_form
        context['search_form_values'] = self.search_form_values
        return context




class ListViewFilterFormMixin(object):
    form_class = ListViewSearchForm
    filter_fields = ()
    cleaned_data = {}
    filter_date_field = 'created'

    def get_queryset(self):
        queryset = super(ListViewFilterFormMixin, self).get_queryset()

        form = self.form_class(self.request.GET)
        if form.is_valid():
            params = {}
            from_date = form.cleaned_data.get('from_date')
            to_date = form.cleaned_data.get('to_date') or date.today()
            keyword = form.cleaned_data.get('keyword')

            if from_date:
                params = {'%s__date__range' % self.filter_date_field: [from_date, to_date]}
            else:
                params = {'%s__date__lte' % self.filter_date_field: to_date}

            queryset = queryset.filter(**params)

            if keyword:
                keyword_list = keyword.split()
                for word in keyword_list:
                    list_of_Q = [Q(**{field+'__icontains': word}) for field in self.filter_fields]
                    queryset = queryset.filter(reduce(operator.or_, list_of_Q))

        self.cleaned_data['from_date'] = form.cleaned_data.get('from_date')
        self.cleaned_data['to_date'] = form.cleaned_data.get('to_date')
        self.cleaned_data['keyword'] = form.cleaned_data.get('keyword')

        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListViewFilterFormMixin, self).get_context_data(**kwargs)

        from_date = self.cleaned_data['from_date']
        to_date = self.cleaned_data['to_date']
        keyword = self.cleaned_data['keyword']

        context['is_filtered'] = from_date or to_date or keyword

        context['form'] = ListViewSearchForm(initial={
            'from_date': from_date, 'to_date': to_date, 'keyword': keyword
        })

        return context
