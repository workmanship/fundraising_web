
import re

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.views.generic.base import View, ContextMixin

from ..decorators import staff_required
from ..utils import convert_camel_case_to_string, load_class_from_string


decorators = [login_required(login_url=reverse('admin:login')), staff_required]


class BackyardPageMixin(object):
    title = ''
    urlpattern = None
    menu_url_name = None

    def get_context_data(self, **kwargs):
        context = super(BackyardPageMixin, self).get_context_data(**kwargs)
        backyard_context = {'page': {
            'title': self.get_title(),
            'object': self,
        }}
        context.update(backyard_context)
        return context

    def get_title(self):
        return self.title or convert_camel_case_to_string(self.__class__.__name__)

    @classmethod
    def get_url_name(cls):
        prefix = ''

        if hasattr(cls, 'parent_cls'):
            cls = cls.parent_cls

        # if hasattr(cls, 'model'):
        #     prefix = '%s_' % cls.model._meta.app_label

        return '%s%s' % (prefix, re.sub('(?!^)([A-Z]+)', r'_\1', cls.__name__).lower())

    @classmethod
    def get_menu_url_name(cls):
        if cls.menu_url_name:
            return cls.menu_url_name

        return cls.get_url_name()

    @classmethod
    def get_url(cls):
        return url(cls.urlpattern, cls.as_view(), name=cls.get_url_name()) if cls.urlpattern else None

    @classmethod
    def url(cls):
        return reverse('backyard:%s' % cls.get_url_name())


@method_decorator(decorators, name='dispatch')
class UserGroupDispatchView(BackyardPageMixin, ContextMixin, View):
    pages = {}

    def _dispatch_to_cls(self, request):
        user_group = ''
        if request.user.groups:
            groups = request.user.groups.values_list('name', flat=True)
            user_group = groups[0] if groups else ''

        view_cls_name = self.pages.get(user_group)

        if not view_cls_name:
            view_cls_name = self.pages.get('')

        return load_class_from_string(view_cls_name)

    def get_context_data(self, **kwargs):
        context = super(UserGroupDispatchView, self).get_context_data(**kwargs)
        view_cls = self._dispatch_to_cls(self.request)
        context['page']['object'] = view_cls
        return context

    def dispatch(self, request, *args, **kwargs):
        view_cls = self._dispatch_to_cls(request)
        return view_cls.as_view()(request, *args, **kwargs)


@method_decorator(decorators, name='dispatch')
class BackyardPage(BackyardPageMixin, ContextMixin, View):
    pass
