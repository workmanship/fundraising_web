from __future__ import unicode_literals

from django.apps import AppConfig


class BackyardConfig(AppConfig):
    name = 'backyard'

    def ready(self):
        super(BackyardConfig, self).ready()
        self.module.register_urls()
