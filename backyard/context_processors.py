from importlib import import_module

from .app_settings import BACKYARD_MENU, BACKYARD_SITE_NAME, BACKYARD_TITLE_HOVER_COLOR
from .utils import load_class_from_string


def backyard(request):
    context = {}

    # Menu
    backyard_menu_config = import_module(BACKYARD_MENU)
    backyard_menu = getattr(backyard_menu_config, 'backyard_menu', {})

    user_group = ''
    if request.user.is_authenticated() and request.user.groups:
        groups = request.user.groups.values_list('name', flat=True)
        user_group = groups[0] if groups else ''

    backyard_menu_by_group = backyard_menu.get(user_group, backyard_menu.get('', []))

    backyard_menu_list = []
    for menu_item in backyard_menu_by_group:
        menu_type = menu_item[0]

        if menu_type in ('link', 'home'):
            cls = load_class_from_string(menu_item[1])

            try:
                icon = menu_item[2]
            except IndexError:
                icon = ''

            backyard_menu_list.append({
                'type': 'link',
                'title': cls().get_title(),
                'url': cls.url(),
                'url_name': cls.get_url_name(),
                'icon': icon,
            })

            if menu_type == 'home':
                context['backyard_home_url'] = cls.url()

        if menu_type == 'section':
            backyard_menu_list.append({
                'type': 'section',
                'title': menu_item[1]
            })

    context['backyard_menu'] = backyard_menu_list

    return context
