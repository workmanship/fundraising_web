# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal

from django import template

register = template.Library()


@register.assignment_tag
def compare_decimal(x, y):
    try:
        return Decimal(x) == Decimal(y)
    except:
        return False
