# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.forms.utils import ErrorList


class RegularForm(forms.Form):
    def add_non_field_error(self, message):
        errors = self._errors.setdefault(forms.forms.NON_FIELD_ERRORS, ErrorList())
        errors.append(message)

    def add_field_error(self, field, message):
        errors = self._errors.setdefault(field, ErrorList())
        errors.append(message)