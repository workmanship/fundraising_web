# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def display_amount(amount):
    if amount is None:
        return '0'

    try:
        float(amount)
    except ValueError:
        return '0'

    if amount % 1:
        try:
            return '{:0,.2f}'.format(amount)
        except ValueError:
            return '0'

    else:
        try:
            return '{:0,.0f}'.format(amount)
        except ValueError:
            return '0'
