# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def is_decimal(x):
    from decimal import Decimal

    try:
        Decimal(x)
    except:
        return False

    return True

