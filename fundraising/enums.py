# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _


class TransactionActionStatus(object):
    PROCESSING = 1
    SUCCESS = 300
    FAILED = 4  # In case of timeout, 404, or 500

    PROCESSING_TEXT = _('Processing')
    SUCCESS_TEXT = _('Success')
    FAILED_TEXT = _('Failed')

    DATA_CHOICES = (
        (PROCESSING, PROCESSING_TEXT),
        (SUCCESS, SUCCESS_TEXT),
        (FAILED, FAILED_TEXT),
    )

    DATA_MAP = {
        PROCESSING: {
            'code': 'processing',
            'text': PROCESSING_TEXT,
        },
        SUCCESS: {
            'code': 'success',
            'text': SUCCESS_TEXT,
        },
        FAILED: {
            'code': 'failed',
            'text': FAILED_TEXT,
        },
    }
