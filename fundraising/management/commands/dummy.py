# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.core.management.base import BaseCommand, CommandError

from fundraising.modules.fund_donation.enums import DonationCampaignStatus
from fundraising.modules.fund_donation.models import DonationCampaign, Donation


class Command(BaseCommand):
    help = 'Create dummy data for testing user interface'

    def handle(self, *args, **options):

        # CAMPAIGNS
        DonationCampaign.objects.create(
            name='UPRIGHT GO | Fix Your Screen-slouch, Correct Your Posture',
            teaser='Easily correct your screen-slouch with a tiny habit-forming wearable. Train, track and improve your posture for sustainable back health',
            description='',
            status=DonationCampaignStatus.ONGOING,
        )
