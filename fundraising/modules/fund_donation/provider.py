# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from fundraising.modules.fund_donation import app_settings
from fundraising.modules.provider import ModuleProvider


class DonationModuleProvider(ModuleProvider):

    def get_provider_code(self):
        return 'donation'

    def get_context_settings(self):
        return {
            'MINIMUM_DONATION_AMOUNT': app_settings.MINIMUM_DONATION_AMOUNT,
            'MAXIMUM_DONATION_AMOUNT': app_settings.MAXIMUM_DONATION_AMOUNT,
            'MINIMUM_DONATION_AMOUNT_FOR_RECEIPT': app_settings.MINIMUM_DONATION_AMOUNT_FOR_RECEIPT,
        }

get_functions = DonationModuleProvider()
