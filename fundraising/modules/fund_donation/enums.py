# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _


class DonationCampaignStatus(object):
    DRAFT = 'draft'
    ONGOING = 'ongoing'
    CLOSED = 'closed'

    # ----- Text -----

    DRAFT_TEXT = _('Draft')
    ONGOING_TEXT = _('Ongoing')
    CLOSED_TEXT = _('Closed')

    DATA_CHOICES = (
        (DRAFT, DRAFT_TEXT),
        (ONGOING, ONGOING_TEXT),
        (CLOSED, CLOSED_TEXT),
    )

    DATA_MAP = {
        DRAFT: {
            'code': DRAFT,
            'text': DRAFT_TEXT,
        },
        ONGOING: {
            'code': ONGOING,
            'text': ONGOING_TEXT,
        },
        CLOSED: {
            'code': CLOSED,
            'text': CLOSED_TEXT,
        },
    }


class DonationType(object):
    ONE_TIME = 'one_time'
    RECURRING = 'recurring'

    # ----- Text -----

    ONE_TIME_TEXT = _('One-time')
    RECURRING_TEXT = _('Recurring')

    DATA_CHOICES = (
        (ONE_TIME, ONE_TIME_TEXT),
        (RECURRING, RECURRING_TEXT),
    )

    DATA_MAP = {
        ONE_TIME: {
            'code': ONE_TIME,
            'text': ONE_TIME_TEXT,
        },
        RECURRING: {
            'code': RECURRING,
            'text': RECURRING_TEXT,
        },
    }


class DonationPaymentStatus(object):
    NEW = 'new'
    PROCESSING = 'processing'

    OFFLINE_PAYMENT_WAITING = 'offline_waiting'
    OFFLINE_PAYMENT_CONFIRMING = 'offline_confirming'
    OFFLINE_PAYMENT_DECLINED = 'offline_declined'

    PAID = 'paid'
    NO_PAYMENT_REQUIRED = 'no_required'

    PAYMENT_FAILED = 'failed'  # Failed at payment gateway
    REFUNDED = 'refunded'
    PARTIALLY_REFUNDED = 'partially_refunded'

    RECURRING = 'recurring'
    RECURRING_STOPPED_BY_STAFF = 'recurring_stop_staff'
    RECURRING_STOPPED_BY_SYSTEM = 'recurring_stop_system'
    RECURRING_STOPPED_BY_USER = 'recurring_stop_user'

    NEW_TEXT = _('New')
    PROCESSING_TEXT = _('Processing')
    OFFLINE_PAYMENT_WAITING_TEXT = _('Offline Payment Waiting')
    OFFLINE_PAYMENT_CONFIRMING_TEXT = _('Offline Payment Confirming')
    OFFLINE_PAYMENT_DECLINED_TEXT = _('Offline Payment Declined')
    PAID_TEXT = _('Paid')
    NO_PAYMENT_REQUIRED_TEXT = _('No Payment Required')
    PAYMENT_FAILED_TEXT = _('Payment Failed')
    REFUNDED_TEXT = _('Refunded')
    PARTIALLY_REFUNDED_TEXT = _('Partially Refunded')
    RECURRING_TEXT = _('Recurring')
    RECURRING_STOPPED_BY_STAFF_TEXT = _('Recurring Stopped by Staff')
    RECURRING_STOPPED_BY_SYSTEM_TEXT = _('Recurring Stopped by System')
    RECURRING_STOPPED_BY_USER_TEXT = _('Recurring Stopped by User')

    DATA_CHOICES = (
        (NEW, NEW_TEXT),
        (PROCESSING, PROCESSING_TEXT),
        (OFFLINE_PAYMENT_WAITING, OFFLINE_PAYMENT_WAITING_TEXT),
        (OFFLINE_PAYMENT_CONFIRMING, OFFLINE_PAYMENT_CONFIRMING_TEXT),
        (OFFLINE_PAYMENT_DECLINED, OFFLINE_PAYMENT_DECLINED_TEXT),
        (PAID, PAID_TEXT),
        (NO_PAYMENT_REQUIRED, NO_PAYMENT_REQUIRED_TEXT),
        (PAYMENT_FAILED, PAYMENT_FAILED_TEXT),
        (REFUNDED, REFUNDED_TEXT),
        (PARTIALLY_REFUNDED, PARTIALLY_REFUNDED_TEXT),
        (RECURRING, RECURRING_TEXT),
        (RECURRING_STOPPED_BY_STAFF, RECURRING_STOPPED_BY_STAFF_TEXT),
        (RECURRING_STOPPED_BY_SYSTEM, RECURRING_STOPPED_BY_SYSTEM_TEXT),
        (RECURRING_STOPPED_BY_USER, RECURRING_STOPPED_BY_USER_TEXT),
    )

    DATA_MAP = {
        NEW: {
            'code': NEW,
            'text': NEW_TEXT,
            'looking': 'good',
        },
        PROCESSING: {
            'code': PROCESSING,
            'text': PROCESSING_TEXT,
            'looking': 'soso',
        },
        OFFLINE_PAYMENT_WAITING: {
            'code': OFFLINE_PAYMENT_WAITING,
            'text': OFFLINE_PAYMENT_WAITING_TEXT,
            'looking': 'soso',
        },
        OFFLINE_PAYMENT_CONFIRMING: {
            'code': OFFLINE_PAYMENT_CONFIRMING,
            'text': OFFLINE_PAYMENT_CONFIRMING_TEXT,
            'looking': 'soso',
        },
        OFFLINE_PAYMENT_DECLINED: {
            'code': OFFLINE_PAYMENT_DECLINED,
            'text': OFFLINE_PAYMENT_DECLINED_TEXT,
            'looking': 'notgood',
        },
        PAID: {
            'code': PAID,
            'text': PAID_TEXT,
            'looking': 'good',
        },
        NO_PAYMENT_REQUIRED: {
            'code': NO_PAYMENT_REQUIRED,
            'text': NO_PAYMENT_REQUIRED_TEXT,
            'looking': 'good',
        },
        PAYMENT_FAILED: {
            'code': PAYMENT_FAILED,
            'text': PAYMENT_FAILED_TEXT,
            'looking': 'notgood',
        },
        REFUNDED: {
            'code': REFUNDED,
            'text': REFUNDED_TEXT,
            'looking': 'notgood',
        },
        PARTIALLY_REFUNDED: {
            'code': PARTIALLY_REFUNDED,
            'text': PARTIALLY_REFUNDED_TEXT,
            'looking': 'notgood',
        },
        RECURRING: {
            'code': RECURRING,
            'text': RECURRING_TEXT,
            'looking': 'good',
        },
        RECURRING_STOPPED_BY_STAFF: {
            'code': RECURRING_STOPPED_BY_STAFF,
            'text': RECURRING_STOPPED_BY_STAFF_TEXT,
            'looking': 'notgood',
        },
        RECURRING_STOPPED_BY_SYSTEM: {
            'code': RECURRING_STOPPED_BY_SYSTEM,
            'text': RECURRING_STOPPED_BY_SYSTEM_TEXT,
            'looking': 'notgood',
        },
        RECURRING_STOPPED_BY_USER: {
            'code': RECURRING_STOPPED_BY_USER,
            'text': RECURRING_STOPPED_BY_USER_TEXT,
            'looking': 'notgood',
        },
    }


class DonationRecurringSequenceStatus(object):
    NEW = 'new'
    PAID = 'paid'
    RETRYING = 'retrying'
    FAILED = 'failed'
    REFUNDED = 'refunded'

    # ----- Text -----

    NEW_TEXT = _('New')
    PAID_TEXT = _('Paid')
    RETRYING_TEXT = _('Retrying')
    FAILED_TEXT = _('Failed')
    REFUNDED_TEXT = _('Refunded')

    DATA_CHOICES = (
        (NEW, NEW_TEXT),
        (PAID, PAID_TEXT),
        (RETRYING, RETRYING_TEXT),
        (FAILED, FAILED_TEXT),
        (REFUNDED, REFUNDED_TEXT),
    )

    DATA_MAP = {
        NEW: {
            'code': NEW,
            'text': NEW_TEXT,
        },
        PAID: {
            'code': PAID,
            'text': PAID_TEXT,
        },
        RETRYING: {
            'code': RETRYING,
            'text': RETRYING_TEXT,
        },
        FAILED: {
            'code': FAILED,
            'text': FAILED_TEXT,
        },
        REFUNDED: {
            'code': REFUNDED,
            'text': REFUNDED_TEXT,
        },
    }










class DonationStatus(object):
    NEW = 0
    IN_PROCESS = 99

    FAILED = 4  # Not visible to donor, Not show in admin (unless specifically queried)

    # One Off
    PAID = 300
    REFUNDED = 102  # Visible to donor, Not count as donation

    # One Off - Transfer
    TRANSFER_WAITING = 111
    TRANSFER_REJECTED = 110

    # Recurring
    RECURRING = 310
    RECURRING_STOPPED_BY_STAFF = 311
    RECURRING_STOPPED_BY_SYSTEM = 312
    RECURRING_STOPPED_BY_USER = 313

    # ----- Helper numbers -----

    """
    How to use Donation Status as a number

    - User dashboard -> More than IS_VISIBLE_TO_DONOR
    - Count as donations -> More than IS_DONATION_CONFIRMED
    - Show in backend -> More than IN_PROCESS
    """

    # Visible in user's dashboard
    # Included -> PAID, REFUNDED, TRANSFER_WAITING, TRANSFER_REJECTED, RECURRING, RECURRING_STOPPED
    IS_VISIBLE_TO_DONOR = 100

    # Considered as a valid donation
    # Included -> PAID, RECURRING, RECURRING_STOPPED
    IS_DONATION_COUNTED = 300

    # ----- Text -----

    NEW_TEXT = _('New')
    IN_PROCESS_TEXT = _('In Process')
    FAILED_TEXT = _('Failed')
    PAID_TEXT = _('Paid')
    REFUNDED_TEXT = _('Refunded')
    TRANSFER_WAITING_TEXT = _('Waiting for Transfer')
    TRANSFER_REJECTED_TEXT = _('Transfer Rejected')
    RECURRING_TEXT = _('Recurring')
    RECURRING_STOPPED_BY_STAFF_TEXT = _('Recurring Stopped by Staff')
    RECURRING_STOPPED_BY_SYSTEM_TEXT = _('Recurring Stopped by System')
    RECURRING_STOPPED_BY_USER_TEXT = _('Recurring Stopped by User')

    DATA_CHOICES = (
        (NEW, NEW_TEXT),
        (IN_PROCESS, IN_PROCESS_TEXT),
        (FAILED, FAILED_TEXT),
        (PAID, PAID_TEXT),
        (REFUNDED, REFUNDED_TEXT),
        (TRANSFER_WAITING, TRANSFER_WAITING_TEXT),
        (TRANSFER_REJECTED, TRANSFER_REJECTED_TEXT),
        (RECURRING, RECURRING_TEXT),
        (RECURRING_STOPPED_BY_STAFF, RECURRING_STOPPED_BY_STAFF_TEXT),
        (RECURRING_STOPPED_BY_SYSTEM, RECURRING_STOPPED_BY_SYSTEM_TEXT),
        (RECURRING_STOPPED_BY_USER, RECURRING_STOPPED_BY_USER_TEXT),
    )

    DATA_MAP = {
        NEW: {
            'code': 'new',
            'text': NEW_TEXT,
        },
        IN_PROCESS: {
            'code': 'in_process',
            'text': IN_PROCESS_TEXT,
        },
        FAILED: {
            'code': 'failed',
            'text': FAILED_TEXT,
        },
        PAID: {
            'code': 'paid',
            'text': PAID_TEXT,
        },
        REFUNDED: {
            'code': 'refunded',
            'text': REFUNDED_TEXT,
        },
        TRANSFER_WAITING: {
            'code': 'waiting',
            'text': TRANSFER_WAITING_TEXT,
        },
        TRANSFER_REJECTED: {
            'code': 'rejected',
            'text': TRANSFER_REJECTED_TEXT,
        },
        RECURRING: {
            'code': 'recurring',
            'text': RECURRING_TEXT,
        },
        RECURRING_STOPPED_BY_STAFF: {
            'code': 'staff_stopped',
            'text': RECURRING_STOPPED_BY_STAFF_TEXT,
        },
        RECURRING_STOPPED_BY_SYSTEM: {
            'code': 'system_stopped',
            'text': RECURRING_STOPPED_BY_SYSTEM_TEXT,
        },
        RECURRING_STOPPED_BY_USER: {
            'code': 'user_stopped',
            'text': RECURRING_STOPPED_BY_USER_TEXT,
        },
    }



