from django.contrib import admin

from .models import Donor, Donation, DonationCampaign, DonationCampaignCategory, DonationCampaignAmount, \
    DonationRecurringInstruction, DonationRecurringSequence

admin.site.register(Donor)

admin.site.register(DonationCampaignCategory)
admin.site.register(DonationCampaign)
admin.site.register(DonationCampaignAmount)

admin.site.register(Donation)

admin.site.register(DonationRecurringInstruction)
admin.site.register(DonationRecurringSequence)
