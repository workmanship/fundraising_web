# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.dispatch import Signal

# For one-off donation
donation_paid = Signal(providing_args=['donation', 'transaction'])
donation_failed = Signal(providing_args=['donation', 'transaction'])

# For bank transfer donation
donation_transfer_waiting = Signal(providing_args=['donation', 'transaction'])
donation_transfer_accept = Signal(providing_args=['donation', 'confirmation'])
donation_transfer_reject = Signal(providing_args=['donation', 'confirmation'])

# For recurring donation
donation_recurring_start = Signal(providing_args=['donation', 'instruction', 'transaction'])
donation_recurring_stop = Signal(providing_args=['donation'])
donation_recurring_cancelled = Signal(providing_args=['donation'])

donation_recurring_paid = Signal(providing_args=['sequence', 'transaction', 'silence'])
donation_recurring_retrying = Signal(providing_args=['sequence', 'transaction'])
donation_recurring_failed = Signal(providing_args=['sequence', 'transaction'])
donation_recurring_refunded = Signal(providing_args=['sequence', 'transaction'])

credit_card_almost_expired = Signal(providing_args=['donation'])
credit_card_expired = Signal(providing_args=['donation'])
