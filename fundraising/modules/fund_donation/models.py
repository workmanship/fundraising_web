# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Sum
from django.urls import reverse
from django.utils import timezone

from autoslug import AutoSlugField
from easy_thumbnails.fields import ThumbnailerImageField
from model_utils.models import TimeStampedModel
from django_fsm import FSMField, transition

from fundraising import app_settings as store_app_settings
from fundraising import constants
from fundraising.enums import TransactionActionStatus
from fundraising.modules.fund_donation import app_settings as donation_app_settings
from fundraising.models import Person, decimalfield_kwargs, ChargeableMixin, RecurringInstructionMixin, \
    AbstractRecurringInstruction, AbstractRecurringSequence
from fundraising.utils.common import split_filepath
from fundraising.utils.interval import extract_interval, display_interval
from fundraising.validators import validate_file_max_size

from . import signals
from .enums import DonationCampaignStatus, DonationStatus, DonationType, \
    DonationPaymentStatus, DonationRecurringSequenceStatus


class Donor(Person):
    class Meta:
        proxy = True

    def do_something(self):
        pass


# DONATION CAMPAIGN ####################################################################################################

class DonationCampaignCategory(models.Model):
    slug = AutoSlugField(populate_from='name', max_length=300, blank=True, editable=True)
    name = models.CharField(max_length=100)
    sorting_order = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['sorting_order']

    def __unicode__(self):
        return self.name


def get_campaign_cover_image_dir(instance, filename):
    (head, name, ext) = split_filepath(filename)
    timestamp = timezone.now().strftime('%Y%m%d-%H%M%S')
    return 'campaigns/{0}/cover-{1}.{2}'.format(instance.id, timestamp, ext)


def get_campaign_fb_share_image_dir(instance, filename):
    (head, name, ext) = split_filepath(filename)
    timestamp = timezone.now().strftime('%Y%m%d-%H%M%S')
    return 'campaigns/{0}/share-fb-{1}.{2}'.format(instance.id, timestamp, ext)


class DonationCampaignQuerySet(models.QuerySet):

    def ongoing_campaigns(self):
        return self.filter(status=DonationCampaignStatus.ONGOING)

    def closed_campaigns(self):
        return self.filter(status=DonationCampaignStatus.CLOSED)


class DonationCampaignManager(models.Manager):
    def get_queryset(self):
        return DonationCampaignQuerySet(self.model, using=self._db)

    def ongoing_campaigns(self):
        return self.get_queryset().ongoing_campaigns()

    def closed_campaigns(self):
        return self.get_queryset().closed_campaigns()


class DonationCampaign(TimeStampedModel):
    slug = AutoSlugField(populate_from='name', max_length=300, blank=True, editable=True)
    name = models.CharField(max_length=300)
    teaser = models.TextField(blank=True)
    description = models.TextField(blank=True)
    category = models.ForeignKey('DonationCampaignCategory', null=True, blank=True, related_name='campaigns')
    status = FSMField(default=DonationCampaignStatus.DRAFT, choices=DonationCampaignStatus.DATA_CHOICES)
    cover_image = ThumbnailerImageField(null=True, blank=True,
                                        upload_to=get_campaign_cover_image_dir,
                                        resize_source=donation_app_settings.THUMBNAIL_SAVE_ORIGINAL['campaign_cover'],
                                        validators=[validate_file_max_size, ])

    objects = DonationCampaignManager()

    def __unicode__(self):
        return '{name} (status: {status})'.format(
            name=self.name,
            status=DonationCampaignStatus.DATA_MAP[self.status]['text'])

    @property
    def status__display(self):
        return DonationCampaignStatus.DATA_MAP[self.status]['text']

    def get_absolute_url(self):
        if self.slug:
            return reverse('donations:donate_campaign_with_slug', args=(self.slug, ))
        else:
            return reverse('donations:donate_campaign', args=(self.id, ))

    @property
    def is_public(self):
        # Note: If closed campaign can still be viewed by website user, add condition below to include 'CLOSED'
        return self.status == DonationCampaignStatus.ONGOING

    @property
    def is_able_to_donate(self):
        return self.status == DonationCampaignStatus.ONGOING


class DonationCampaignAmount(models.Model):
    campaign = models.ForeignKey('DonationCampaign', related_name='amount_choices', null=True, blank=True)
    amount = models.DecimalField(validators=[MinValueValidator(donation_app_settings.MINIMUM_DONATION_AMOUNT)],
                                 **decimalfield_kwargs)
    description = models.CharField(max_length=512, blank=True, null=True, default='')
    sorting_order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ('sorting_order', )


# DONATION #############################################################################################################

def generate_donation_number():
    from django.db import transaction
    from sequences import get_next_value

    with transaction.atomic():
        format_str = '{{:0{0}}}'.format(donation_app_settings.DONATION_NUMBER_LENGTH)
        next_number = get_next_value('donations')
        return format_str.format(next_number)


class DonationQuerySet(models.QuerySet):

    def confirmed_donations(self):
        return self.filter(is_confirmed=True)


class DonationManager(models.Manager):
    def get_queryset(self):
        return DonationQuerySet(self.model, using=self._db)

    def confirmed_donations(self):
        return self.get_queryset().confirmed_donations()


class Donation(ChargeableMixin, TimeStampedModel):
    person = models.ForeignKey('fundraising.Person', related_name='donations')

    donation_number = models.CharField(max_length=40, unique=True, db_index=True, null=True, blank=True)
    campaign = models.ForeignKey(DonationCampaign, null=True)

    donation_type = models.CharField(max_length=20, choices=DonationType.DATA_CHOICES)
    amount = models.DecimalField(**decimalfield_kwargs)
    cumulative_amount = models.DecimalField(default=0, **decimalfield_kwargs)
    currency = models.CharField(max_length=7, choices=constants.CURRENCY, default=store_app_settings.DEFAULT_CURRENCY)

    donor_first_name = models.CharField(max_length=200, null=True, blank=True)
    donor_last_name = models.CharField(max_length=200, null=True, blank=True)
    donor_phone_number = models.CharField(max_length=100, null=True, blank=True)
    donor_email = models.EmailField(max_length=200, null=True, blank=True)
    receipt_address = models.CharField(max_length=500, null=True, blank=True)

    date_submitted = models.DateTimeField(null=True)  # Date in which donation is submitted by user
    payment_status = FSMField(default=DonationPaymentStatus.NEW)
    is_confirmed = models.BooleanField(default=False)  # This donation can be accounted for
    cancel_reason = models.CharField(max_length=300, null=True, blank=True)

    paid_transaction = models.OneToOneField('fundraising.StoreTransaction', null=True, related_name='paid_donation')
    payment_provider = models.CharField(max_length=30, blank=True)
    payment_method = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return u'{type} Donation of {amount} {currency} from {donor_fullname} (#{donation_number})'.format(
            type=DonationType.DATA_MAP[self.donation_type]['text'], amount=self.amount, currency=self.currency,
            donor_fullname=self.donor_fullname_or_anonymous, donation_number=self.donation_number)

    objects = DonationManager()

    @property
    def donor_fullname(self):
        if self.donor_first_name or self.donor_last_name:
            return '{0}{1}'.format(self.donor_first_name, ' {}'.format(self.donor_last_name) if self.donor_last_name else '')
        else:
            return self.person.fullname

    @property
    def donor_fullname_or_anonymous(self):
        return self.donor_fullname if self.donor_fullname else donation_app_settings.ANONYMOUS_DONOR_TEXT

    @property
    def charge_amount(self):
        return self.amount

    @property
    def is_recurring(self):
        return self.donation_type == DonationType.RECURRING

    @property
    def recurring_interval__display(self):
        try:
            return self.recurring_instruction.interval__display
        except DonationRecurringInstruction.DoesNotExist:
            return ''

    @property
    def donation_type__display(self):
        return DonationType.DATA_MAP[self.donation_type]['text']

    @property
    def payment_status__display(self):
        return DonationStatus.DATA_MAP[self.payment_status]['text']

    @property
    def payment_method__display(self):
        payment_provider = self.get_payment_provider()
        return payment_provider.get_payment_method_name(self.payment_method)

    # Payment Functions

    def get_payment_provider(self):
        from fundraising.payments.providers import get_payment_provider
        return get_payment_provider(self.payment_provider)

    # Status : NEW => PROCESSING
    @transition(field=payment_status,
                source=DonationPaymentStatus.NEW,
                target=DonationPaymentStatus.PROCESSING)
    def set_payment_status_processing(self):
        pass

    # Status : NEW => OFFLINE_PAYMENT_WAITING
    @transition(field=payment_status,
                source=DonationPaymentStatus.NEW,
                target=DonationPaymentStatus.OFFLINE_PAYMENT_WAITING)
    def set_payment_status_waiting(self):
        pass

    # Status : OFFLINE_PAYMENT_WAITING => OFFLINE_PAYMENT_CONFIRMING
    @transition(field=payment_status,
                source=DonationPaymentStatus.OFFLINE_PAYMENT_WAITING,
                target=DonationPaymentStatus.OFFLINE_PAYMENT_CONFIRMING)
    def set_payment_status_confirming(self):
        pass

    # Status : OFFLINE_PAYMENT_CONFIRMING => OFFLINE_PAYMENT_DECLINED
    @transition(field=payment_status,
                source=DonationPaymentStatus.OFFLINE_PAYMENT_CONFIRMING,
                target=DonationPaymentStatus.OFFLINE_PAYMENT_DECLINED)
    def set_payment_status_declined(self):
        pass

    # Status : NEW => NO_PAYMENT_REQUIRED
    @transition(field=payment_status,
                source=DonationPaymentStatus.NEW,
                target=DonationPaymentStatus.NO_PAYMENT_REQUIRED)
    def set_payment_status_no_required(self):
        self.is_confirmed = True

    # Status : NEW or PROCESSING or OFFLINE_PAYMENT_CONFIRMING => PAID
    @transition(field=payment_status,
                source=(DonationPaymentStatus.NEW,
                        DonationPaymentStatus.PROCESSING,
                        DonationPaymentStatus.OFFLINE_PAYMENT_CONFIRMING),
                target=DonationPaymentStatus.PAID)
    def set_payment_status_paid(self, transaction):
        self.donation_number = generate_donation_number()
        self.paid_transaction = transaction.to_store_transaction()
        self.cumulative_amount = transaction.amount
        self.is_confirmed = True

    # Status : NEW or PROCESSING or WAITING_FOR_PAYMENT => PAYMENT_FAILED
    @transition(field=payment_status,
                source=(DonationPaymentStatus.NEW,
                        DonationPaymentStatus.PROCESSING),
                target=DonationPaymentStatus.PAYMENT_FAILED)
    def set_payment_status_failed(self):
        pass

    # Status : PAID => REFUNDED
    @transition(field=payment_status,
                source=DonationPaymentStatus.PAID,
                target=DonationPaymentStatus.REFUNDED)
    def set_payment_status_refunded(self):
        pass

    # Status : PAID => PARTIALLY_REFUNDED
    @transition(field=payment_status,
                source=DonationPaymentStatus.PAID,
                target=DonationPaymentStatus.PARTIALLY_REFUNDED)
    def set_payment_status_partially_refunded(self):
        pass

    # Status : NEW or PROCESSING => RECURRING
    @transition(field=payment_status,
                source=(DonationPaymentStatus.NEW, DonationPaymentStatus.PROCESSING),
                target=DonationPaymentStatus.RECURRING)
    def set_payment_status_recurring(self, instruction, first_paid_transaction=None, silence=False):
        self.donation_number = generate_donation_number()
        signals.donation_recurring_start.send(sender=self.__class__, donation=self, instruction=instruction,
                                              transaction=first_paid_transaction)

    # Status : RECURRING => RECURRING_STOPPED_BY_STAFF
    @transition(field=payment_status,
                source=DonationPaymentStatus.RECURRING,
                target=DonationPaymentStatus.RECURRING_STOPPED_BY_STAFF)
    def set_payment_status_stopped_by_staff(self, reason):
        self.cancel_reason = reason

    # Status : RECURRING => RECURRING_STOPPED_BY_SYSTEM
    @transition(field=payment_status,
                source=DonationPaymentStatus.RECURRING,
                target=DonationPaymentStatus.RECURRING_STOPPED_BY_SYSTEM)
    def set_payment_status_stopped_by_system(self, reason):
        self.cancel_reason = reason

    # Status : RECURRING => RECURRING_STOPPED_BY_USER
    @transition(field=payment_status,
                source=DonationPaymentStatus.RECURRING,
                target=DonationPaymentStatus.RECURRING_STOPPED_BY_USER)
    def set_payment_status_stopped_by_user(self, reason):
        self.cancel_reason = reason


# Recurring
# ----------------------------------------------------------------------------------------------------------------------

class DonationRecurringInstruction(RecurringInstructionMixin, AbstractRecurringInstruction):
    instruction = models.OneToOneField('fundraising.StoreRecurringInstruction', related_name='donation_instruction')

    donation = models.OneToOneField('Donation', related_name='recurring_instruction')
    interval = models.CharField(max_length=10)
    is_active = models.BooleanField(default=False)  # Should related to donation.payment_status

    def __unicode__(self):
        interval_num, interval_type = extract_interval(self.interval)
        return 'Recurring every {0} {1}'.format(interval_num, interval_type)

    @property
    def chargeable(self):
        return self.donation

    @property
    def interval__display(self):
        return display_interval(self.interval)

    # Functions

    def activate(self):
        self.is_active = True
        self.save()

    def stop(self):
        self.reset_sequences()

        self.is_active = False
        self.save()

    def get_or_create_first_sequence(self):
        sequence = DonationRecurringSequence.objects.filter(instruction=self).order_by('due_date').first()

        if not sequence:
            interval_num, interval_type = extract_interval(self.interval)
            now = timezone.now()

            if interval_type == 'minutes':
                a, b = divmod(now.minute, interval_num)
                now = now.replace(second=0, microsecond=0)
                due_date = now + relativedelta(minutes=interval_num - b)

            else:
                due_date = now.replace(hour=0, minute=0, second=0, microsecond=0)\
                             + relativedelta(**{interval_type: interval_num})

            sequence = DonationRecurringSequence.objects.create(instruction=self, due_date=due_date)

        return sequence

    def generate_next_recurring_sequences(self):
        interval_num, interval_type = extract_interval(self.interval)
        now = timezone.now()

        if interval_type == 'minutes':
            """
            Sequence in minutes is intended to use while in development only
            """

            a, b = divmod(now.minute, interval_num)
            now = now.replace(second=0, microsecond=0)
            start_datetime = now + relativedelta(minutes=interval_num - b)

            for number in range(donation_app_settings.RECURRING_SEQUENCES_IN_ADVANCE):
                due_datetime = start_datetime + relativedelta(minutes=(number+1) * interval_num)
                DonationRecurringSequence.objects.get_or_create(instruction=self, due_date=due_datetime)

        else:
            """
            Generage sequence to charge on 'RECURRING_CHARGE_DATE' every month
            """

            start_date = now.replace(
                day=donation_app_settings.RECURRING_CHARGE_DATE, hour=0, minute=0, second=0, microsecond=0
            ) + relativedelta(**{interval_type: interval_num})

            for counter in range(donation_app_settings.RECURRING_SEQUENCES_IN_ADVANCE):
                due_date = start_date + relativedelta(**{interval_type: interval_num * counter})
                DonationRecurringSequence.objects.get_or_create(instruction=self, due_date=due_date)

    def reset_sequences(self):
        self.sequences.filter(status=DonationRecurringSequenceStatus.NEW, due_date__gte=timezone.now().date()).delete()


class DonationRecurringSequenceQuerySet(models.QuerySet):
    def is_due(self):
        now = timezone.now()

        if settings.DEBUG:
            due_datetime = now.replace(second=0, microsecond=0)
        else:
            due_datetime = now.replace(hour=0, minute=0, second=0, microsecond=0)

        return self.filter(due_date=due_datetime, instruction__is_active=True,
                           status__in=[DonationRecurringSequenceStatus.NEW,
                                       DonationRecurringSequenceStatus.RETRYING]
                           )

    def is_paid(self):
        return self.filter(status=DonationRecurringSequenceStatus.PAID)

    def new_to_old(self):
        return self.order_by('-due_date')

    def old_to_new(self):
        return self.order_by('due_date')


class DonationRecurringSequenceManager(models.Manager):
    def get_queryset(self):
        return DonationRecurringSequenceQuerySet(self.model, using=self._db)

    def is_due(self):
        return self.get_queryset().is_due()

    def is_paid(self):
        return self.get_queryset().is_paid()

    def new_to_old(self):
        return self.get_queryset().new_to_old()

    def old_to_new(self):
        return self.get_queryset().old_to_new()


class DonationRecurringSequence(AbstractRecurringSequence):
    sequence = models.OneToOneField('fundraising.StoreRecurringSequence', related_name='donation_sequence')

    instruction = models.ForeignKey('DonationRecurringInstruction', related_name='sequences')
    due_date = models.DateTimeField()

    status = FSMField(default=DonationRecurringSequenceStatus.NEW, choices=DonationRecurringSequenceStatus.DATA_CHOICES)
    retries = models.PositiveSmallIntegerField(default=0)

    cancel_reason = models.CharField(max_length=300, null=True, blank=True)
    paid_transaction = models.OneToOneField('fundraising.StoreTransaction', null=True,
                                            related_name='donation_sequence_paid_sequence')

    objects = DonationRecurringSequenceManager()

    @property
    def store_recurring_instruction(self):
        return self.instruction.instruction

    def get_next_retry(self):
        """
        Get date on which recurring payment should be retried when charging was failed.
        Developers can customize this function by project's retry policy.
        :return: Date
        """

        if self.retries >= donation_app_settings.RECURRING_RETRY_ATTEMPTS:
            return None

        if settings.DEBUG:
            next_due_date = self.due_date + relativedelta(minutes=1)
        else:
            next_due_date = self.due_date + relativedelta(days=1)

        return next_due_date

    @property
    def refundable_amount(self):
        refundable_amount = 0

        if self.paid_transaction and self.status == \
                (DonationRecurringSequenceStatus.PAID or DonationRecurringSequenceStatus.REFUNDED):
            refunded_amount = self.sequence.transactions.filter(
                transaction__action='refund',
                transaction__action_status=TransactionActionStatus.SUCCESS
            ).aggregate(Sum('transaction__amount'))['transaction__amount__sum'] or 0
            refundable_amount = self.paid_transaction.amount - refunded_amount

        return refundable_amount

    @property
    def can_charge(self):
        return self.instruction.is_active and \
               self.status in [DonationRecurringSequenceStatus.NEW, DonationRecurringSequenceStatus.RETRYING]

    # Status : NEW => PAID
    @transition(field=status, source=DonationRecurringSequenceStatus.NEW,
                target=DonationRecurringSequenceStatus.PAID)
    def set_status_paid(self, paid_transaction, silence=False):
        self.paid_transaction = paid_transaction.to_store_transaction()

        # Update donation cumulative amount
        self.instruction.donation.cumulative_amount += paid_transaction.amount
        self.instruction.donation.save()

        # TODO : Update total donor and donation

        signals.donation_recurring_paid.send(
            sender=self.__class__, sequence=self, paid_transaction=paid_transaction, silence=silence)

    # Status : NEW => RETRYING
    @transition(field=status, source=DonationRecurringSequenceStatus.NEW,
                target=DonationRecurringSequenceStatus.RETRYING)
    def set_status_retrying(self, transaction, next_due_date, silence=False):
        self.due_date = next_due_date
        self.retries += 1

        signals.donation_recurring_retrying.send(
            sender=self.__class__, sequence=self, transaction=transaction, silence=silence)

    # Status : NEW => FAILED
    @transition(field=status, source=DonationRecurringSequenceStatus.NEW,
                target=DonationRecurringSequenceStatus.FAILED)
    def set_status_failed(self, transaction, silence=False):
        signals.donation_recurring_failed.send(
            sender=self.__class__, sequence=self, transaction=transaction, silence=silence)

    # Status : PAID => REFUNDED
    @transition(field=status, source=DonationRecurringSequenceStatus.PAID,
                target=DonationRecurringSequenceStatus.REFUNDED)
    def set_status_refunded(self, refund_transaction, silence=False):

        # Update donation cumulative amount
        self.instruction.donation.cumulative_amount -= refund_transaction.amount
        self.instruction.donation.save()

        # TODO : Update total donor and donation

        signals.donation_recurring_refunded.send(
            sender=self.__class__, sequence=self, transaction=refund_transaction, silence=silence)
