# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

import shortuuid

from fundraising.constants import SHORTUUID_NUMBER_ONLY


def generate_with_date_and_random(length, prefix=''):
    shortuuid.set_alphabet(SHORTUUID_NUMBER_ONLY)
    today = date.today()
    return '{prefix}{date:%Y%m%d}_{number}'.format(prefix=prefix, date=today, number=shortuuid.uuid()[:length])


def generate_with_serial_number(prefix, length):
    # TODO
    pass
