# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-26 10:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fund_donation', '0005_auto_20170311_1452'),
    ]

    operations = [
        migrations.AddField(
            model_name='donation',
            name='is_confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='donation',
            name='currency',
            field=models.CharField(choices=[('THB', 'THB'), ('USD', 'USD')], default='THB', max_length=7),
        ),
    ]
