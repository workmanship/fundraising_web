# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from fundraising.exceptions import ActionNotPermittedException
from website.celery import app


@app.task(bind=True)
def charge_recurring_donation(self):
    from fundraising.modules.fund_donation.models import DonationRecurringSequence
    from . import functions as donation_fn

    for sequence in DonationRecurringSequence.objects.is_due():
        try:
            donation_fn.charge_recurring_donation(sequence)
        except ActionNotPermittedException:
            pass


@app.task(bind=True)
def generate_sequences_from_instruction(self):
    from fundraising.modules.fund_donation.models import DonationRecurringInstruction

    for instruction in DonationRecurringInstruction.objects.filter(is_active=True):
        instruction.generate_next_recurring_sequences()
