# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig

from fundraising import register_module


class DonationModuleConfig(AppConfig):
    name = 'fundraising.modules.fund_donation'
    verbose_name = 'Donation Module'

    def ready(self):
        from .provider import DonationModuleProvider
        register_module(DonationModuleProvider())


default_app_config = 'fundraising.modules.fund_donation.DonationModuleConfig'
