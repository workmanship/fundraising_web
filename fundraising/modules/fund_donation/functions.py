# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from fundraising.enums import TransactionActionStatus
from fundraising.exceptions import ActionNotPermittedException

from fundraising.models import Person
from fundraising.modules.fund_donation import app_settings as donation_app_settings
from fundraising.modules.fund_donation.enums import DonationType, DonationRecurringSequenceStatus, DonationPaymentStatus
from fundraising.payments.providers import get_payment_provider

from .models import Donation, DonationCampaignAmount, DonationRecurringInstruction


def get_donation_amount_choices(campaign=None):
    if campaign:
        choices = DonationCampaignAmount.objects.filter(campaign=campaign)
    else:
        choices = None

    if not choices:
        choices = DonationCampaignAmount.objects.filter(campaign=None)

    if not choices:
        choices = [DonationCampaignAmount(
            campaign=campaign,
            amount=amount,
            description='',
            sorting_order=0
        ) for amount in donation_app_settings.DONATION_AMOUNT_CHOICES]

    return choices


def get_or_update_donor(request, first_name, last_name, email, phone_number, receipt_address):
    if request.user.is_authenticated():
        person, created = Person.objects.get_or_create(user=request.user, defaults={
            'first_name': first_name,
            'last_name': last_name,
            'email': request.user.email if request.user.email else email,
            'phone_number': phone_number,
            'receipt_address': receipt_address,
        })

    else:
        try:
            person = Person.objects.get(first_name=first_name, last_name=last_name, email=email)
        except Person.DoesNotExist:
            person = Person.objects.create(
                first_name=first_name,
                last_name=last_name,
                email=email,
                phone_number=phone_number,
                receipt_address=receipt_address,
            )
        else:
            # Update person
            person.phone_number = phone_number if phone_number else person.phone_number
            person.receipt_address = receipt_address if receipt_address else person.receipt_address
            person.save()

    return person


def new_donation(amount, person, campaign=None, recurring_interval=None):
    if recurring_interval:
        donation_type = DonationType.RECURRING
    else:
        donation_type = DonationType.ONE_TIME

    now = timezone.now()

    donation = Donation.objects.create(
        person=person,
        campaign=campaign,
        donation_type=donation_type,
        amount=amount,
        date_submitted=now,

        donor_first_name=person.first_name,
        donor_last_name=person.last_name,
        donor_email=person.email,
        donor_phone_number=person.phone_number,
        receipt_address=person.receipt_address,
    )

    if recurring_interval:
        DonationRecurringInstruction.objects.create(donation=donation, interval=recurring_interval)

    return donation


def start_recurring(donation, first_paid_transaction=None):
    instruction = DonationRecurringInstruction.objects.get(donation=donation)
    instruction.generate_next_recurring_sequences()
    instruction.activate()

    donation.set_payment_status_recurring(instruction, first_paid_transaction)
    donation.save()


def charge_recurring_donation(sequence):
    if not sequence.instruction.is_active or sequence.status not in (
            DonationRecurringSequenceStatus.NEW,
            DonationRecurringSequenceStatus.RETRYING):
        raise ActionNotPermittedException(_('This donation may have already been paid.'))

    provider = get_payment_provider(sequence.instruction.chargeable.payment_provider)
    charge_transaction = provider.charge_recurring_sequence(sequence)

    if charge_transaction.action_status == TransactionActionStatus.SUCCESS:
        sequence.set_status_paid(charge_transaction)
        sequence.save()

        print(' >>>>> [charge_recurring_donation] CHARGE SUCCESS ')

    elif charge_transaction.action_status == TransactionActionStatus.FAILED:
        try:
            is_permanent_failed = charge_transaction.is_permanent_failed
        except AttributeError:
            is_permanent_failed = False

        if is_permanent_failed:
            # Stop donation
            donation = sequence.instruction.donation
            donation.set_payment_status_stopped_by_system(charge_transaction.fail_code)
            donation.save()

        else:
            next_due_date = sequence.get_next_retry()

            if next_due_date:
                sequence.set_status_retrying(charge_transaction, next_due_date)
                sequence.save()

            else:
                sequence.set_status_failed(transaction=charge_transaction)
                sequence.save()

                donation = sequence.instruction.donation
                donation.set_payment_status_stopped_by_system(reason='EXCEED_RECURRING_MAXIMUM_CHARGE_ATTEMPT')
                donation.save()

    return charge_transaction


def update_recurring_donation(donation, amount, interval):
    donation.amount = amount
    donation.save()

    instruction = donation.recurring_instruction
    if instruction.interval != interval:
        instruction.reset_sequences()
        instruction.generate_next_recurring_sequences()

    instruction.interval = interval
    instruction.save()


def refund_donation(donation, refund_amount=None, sequence=None):
    # TODO : Support refund_amount

    if donation.donation_type == DonationType.ONE_TIME:
        if donation.payment_status != DonationPaymentStatus.PAID:
            raise ActionNotPermittedException(_('This donation is either refunded or not valid for refund.'))

        paid_transaction = donation.paid_transaction
        provider = get_payment_provider(paid_transaction.payment_provider)

        try:
            refunded_transaction = provider.refund_transaction(paid_transaction)
        except NotImplementedError:
            raise ActionNotPermittedException(_('This payment method does not allow refund.'))

        donation.set_payment_status_refunded()
        donation.save()

    elif donation.donation_type == DonationType.RECURRING:
        if sequence:
            if sequence.status != DonationRecurringSequenceStatus.PAID:
                raise ActionNotPermittedException(_('This donation is either refunded or not valid for refund.'))

            paid_transaction = sequence.paid_transaction
            provider = get_payment_provider(paid_transaction.payment_provider)

            try:
                refunded_transaction = provider.refund_transaction(paid_transaction)
            except NotImplementedError:
                raise ActionNotPermittedException(_('This payment method does not allow refund.'))

            sequence.set_status_refunded()
            sequence.save()

        else:
            # TODO : Support refund all sequences
            refunded_transaction = None

    else:
        refunded_transaction = None

    return refunded_transaction
