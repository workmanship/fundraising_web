# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal

from django.conf import settings
from django.utils.translation import ugettext_lazy as _


# Donation Number
# ----------------------------------------------------------------------------------------------------------------------

DONATION_NUMBER_PREFIX = getattr(settings, 'DONATION_NUMBER_PREFIX', 'WEB_')
DONATION_NUMBER_LENGTH = getattr(settings, 'DONATION_NUMBER_LENGTH', 9)


# Donation Amount
# ----------------------------------------------------------------------------------------------------------------------

MINIMUM_DONATION_AMOUNT = getattr(settings, 'MINIMUM_DONATION_AMOUNT', 20)
MAXIMUM_DONATION_AMOUNT = getattr(settings, 'MAXIMUM_DONATION_AMOUNT', 1000000)  # TODO
DONATION_AMOUNT_CHOICES = getattr(settings, 'DONATION_AMOUNT_CHOICES', (Decimal(2000), Decimal(1000), Decimal(500)))

MINIMUM_DONATION_AMOUNT_FOR_RECEIPT = getattr(settings, 'MINIMUM_DONATION_AMOUNT_FOR_RECEIPT', 2000)


# Donor
# ----------------------------------------------------------------------------------------------------------------------

ANONYMOUS_DONOR_TEXT = getattr(settings, 'DONATION_ANONYMOUS_DONOR_TEXT', _('Anonymous'))


# Campaign Cover Image
# ----------------------------------------------------------------------------------------------------------------------

THUMBNAIL_SAVE_ORIGINAL = getattr(settings, 'DONATION_THUMBNAIL_SAVE_ORIGINAL', {
    'campaign_cover': {'size': (1600, 1200)},
    'campaign_fb_share': {'size': (2214, 1160)},
})


# Recurring Donations
# ----------------------------------------------------------------------------------------------------------------------

RECURRING_CHARGE_DATE = getattr(settings, 'DONATION_RECURRING_CHARGE_DATE', 5)
RECURRING_CHARGE_TIME = getattr(settings, 'DONATION_RECURRING_CHARGE_TIME', 8)  # Time in 24 hours format

RECURRING_SEQUENCES_IN_ADVANCE = getattr(settings, 'DONATION_RECURRING_SEQUENCES_IN_ADVANCE', 6)
RECURRING_RETRY_ATTEMPTS = getattr(settings, 'DONATION_RECURRING_RETRY_ATTEMPTS', 3)
