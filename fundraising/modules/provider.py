# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class ModuleProvider(object):
    """
    Base class for Module Providers
    """

    def get_provider_code(self):
        raise NotImplementedError

    def get_context_settings(self):
        return {}
