import importlib

from django.conf.urls import include, url

from . import module_providers, payment_providers

urlpatterns = []

for payment_code in payment_providers.keys():
    try:
        i = importlib.import_module('fundraising.payments.%s.urls' % payment_code)
        urlpatterns += [url(r'^', include(i, namespace=payment_code))]
    except ImportError:
        pass

for module_code in module_providers.keys():
    try:
        i = importlib.import_module('fundraising.modules.%s.urls' % module_code)
        urlpatterns += [url(r'^', include(i, namespace=module_code))]
    except ImportError:
        pass
