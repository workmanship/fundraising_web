# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from . import payment_providers, module_providers


def fundraising(request):
    context_settings = {}

    for payment_code in payment_providers.keys():
        provider = payment_providers[payment_code]
        context_settings.update(provider.get_context_settings())

    for module_code in module_providers.keys():
        provider = module_providers[module_code]
        context_settings.update(provider.get_context_settings())

    return context_settings
