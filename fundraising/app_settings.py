from django.conf import settings


APP_LABEL = getattr(settings, 'FUNDRAISING_APP_LABEL', '')

DEFAULT_CURRENCY = getattr(settings, 'FUNDRAISING_DEFAULT_CURRENCY', 'THB')

MAX_COVER_UPLOAD_SIZE = getattr(settings, 'FUNDRAISING_MAX_COVER_UPLOAD_SIZE', 5242880)
MAX_COVER_UPLOAD_TEXT = getattr(settings, 'FUNDRAISING_MAX_COVER_UPLOAD_TEXT', '5MB')






PAYMENT_THANKYOU_REDIRECT_URL = getattr(settings, 'FUNDRAISING_PAYMENT_THANKYOU_REDIRECT_URL', '')
PAYMENT_FAILED_REDIRECT_URL = getattr(settings, 'FUNDRAISING_PAYMENT_FAILED_REDIRECT_URL', '')
PAYMENT_CANCELLED_REDIRECT_URL = getattr(settings, 'FUNDRAISING_PAYMENT_CANCELLED_REDIRECT_URL', '')
