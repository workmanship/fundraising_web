# -*- coding: utf-8 -*-
from __future__ import unicode_literals


# Modules Providers
# ----------------------------------------------------------------------------------------------------------------------

module_providers = {}


def register_module(module_provider):
    module_providers[module_provider.get_provider_code()] = module_provider


# Payment Providers
# ----------------------------------------------------------------------------------------------------------------------

payment_providers = {}
valid_payment_options = []


def register_provider(provider):
    payment_providers[provider.provider_code] = provider
    for method in provider.payment_methods:
        valid_payment_options.append('{0}:{1}'.format(provider.provider_code, method))
