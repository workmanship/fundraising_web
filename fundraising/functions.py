# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from fundraising.models import Person

"""
def get_person(request, email=None):
    if request.user.is_authenticated():
        # If authenticated, return existing donor
        try:
            person = Person.objects.get(user=request.user)
        except Person.DoesNotExist:
            raise Person.DoesNotExist

        return person

    else:
        # If not authenticated, return existing donor only if email is matched
        if email:
            try:
                person = Person.objects.get(email=email)
            except Person.DoesNotExist:
                raise Person.DoesNotExist

            return person

        else:
            raise Person.DoesNotExist


def create_person(request, **person_fields):

    person = Person.objects.create(
        user=request.user if request.user.is_authenticated() else None,
        **person_fields
    )

    return person
"""
