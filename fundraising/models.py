# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import shortuuid
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from model_utils.models import TimeStampedModel

from fundraising.enums import TransactionActionStatus
from . import app_settings, constants, db
from .utils.common import generate_uuid_with_today


decimalfield_kwargs = {
    'max_digits': 30,
    'decimal_places': 2,
}


class Person(models.Model):
    uuid = models.CharField(max_length=50)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='person')

    # Following info will be stored only for authenticated user

    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    phone_number = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)

    receipt_address = models.CharField(max_length=500, null=True, blank=True)

    def __unicode__(self):
        return self.fullname

    def save(self, *args, **kwargs):
        if not self.uuid:
            self.user_uuid = shortuuid.uuid()

        super(Person, self).save(*args, **kwargs)

    @property
    def fullname(self):
        return '{0}{1}'.format(self.first_name, ' {}'.format(self.last_name) if self.last_name else '')


# Chargeable
# ----------------------------------------------------------------------------------------------------------------------

class ChargeableMixin(object):
    def __str__(self):
        raise NotImplementedError

    def charge_amount(self):
        raise NotImplementedError

    def currency(self):
        raise NotImplementedError


# Transaction
# ----------------------------------------------------------------------------------------------------------------------

def generate_transaction_number():
    return generate_uuid_with_today()


class StoreTransaction(db.GenericModel):
    def to_store_transaction(self):
        return self


class AbstractTransaction(TimeStampedModel):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    chargeable = GenericForeignKey('content_type', 'object_id')

    transaction_number = models.CharField(max_length=50, null=True, blank=True)
    amount = models.DecimalField(max_digits=30, decimal_places=2)
    currency = models.CharField(max_length=4, choices=constants.CURRENCY, default=app_settings.DEFAULT_CURRENCY)

    payment_provider = models.CharField(max_length=30)
    payment_method = models.CharField(max_length=30, null=True, blank=True)

    action = models.CharField(max_length=30)
    action_status = models.PositiveSmallIntegerField(choices=TransactionActionStatus.DATA_CHOICES,
                                                     default=TransactionActionStatus.PROCESSING)
    fail_code = models.CharField(max_length=200, blank=True, null=True)
    fail_message = models.CharField(max_length=1000, blank=True, null=True)

    request_data = models.TextField(blank=True, null=True)
    response_data = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id:
            self.transaction = StoreTransaction.objects.create(child=self._meta.get_field('transaction').related_query_name())
            self.transaction_number = generate_transaction_number()
        super(AbstractTransaction, self).save(*args, **kwargs)

    def to_store_transaction(self):
        return self.transaction


# Recurring
# ----------------------------------------------------------------------------------------------------------------------

class RecurringInstructionMixin(object):
    def chargeable(self):
        raise NotImplementedError

    def activate(self):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError


class StoreRecurringInstruction(db.GenericModel):
    def to_store_recurring_instruction(self):
        return self


class AbstractRecurringInstruction(TimeStampedModel):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id:
            instruction = StoreRecurringInstruction.objects.create(
                child=self._meta.get_field('instruction').related_query_name())
            self.instruction = instruction
        super(AbstractRecurringInstruction, self).save(*args, **kwargs)

    def to_store_recurring_instruction(self):
        return self.instruction


class StoreRecurringSequence(db.GenericModel):
    def to_store_recurring_sequence(self):
        return self


class AbstractRecurringSequence(TimeStampedModel):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.id:
            sequence = StoreRecurringSequence.objects.create(child=self._meta.get_field('sequence').related_query_name())
            self.sequence = sequence
        super(AbstractRecurringSequence, self).save(*args, **kwargs)

    def to_store_recurring_sequence(self):
        return self.sequence

    @property
    def store_recurring_instruction(self):
        raise NotImplementedError


class StoreRecurringSequenceTransaction(models.Model):
    sequence = models.ForeignKey('fundraising.StoreRecurringSequence', related_name='transactions')
    transaction = models.OneToOneField('fundraising.StoreTransaction', related_name='recurring_transaction')
