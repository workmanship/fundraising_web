# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from fundraising.app_settings import MAX_COVER_UPLOAD_SIZE, MAX_COVER_UPLOAD_TEXT
from fundraising.utils.common import split_filepath


def validate_file_extension_html(value):
    _, _, ext = split_filepath(value.name)
    valid_extensions = ['html']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))


def validate_file_max_size(value):
    if value.size > MAX_COVER_UPLOAD_SIZE:
        raise ValidationError(_('File size must not larger than {0}').format(MAX_COVER_UPLOAD_TEXT))
