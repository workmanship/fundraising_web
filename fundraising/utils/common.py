# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from datetime import date

import shortuuid

from django.conf import settings


def generate_uuid_with_today():
    today = date.today()
    return '{:%Y%m%d}-{}'.format(today, shortuuid.uuid())


def split_filepath(path):
    (head, tail) = os.path.split(path)
    (root, ext) = os.path.splitext(tail)

    if ext and ext[0] == '.':
        ext = ext[1:]

    return head, root, ext


def get_absolute_url_from_view_name(view_name):
    return '{domain}{view_name}'.format(domain=settings.WEBSITE_ROOT, view_name=view_name)
