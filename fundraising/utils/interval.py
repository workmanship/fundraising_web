# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re

from django.utils.translation import ugettext_lazy as _

INTERVAL_DURATION = {
    'd': 'days',
    'w': 'weeks',
    'm': 'months',
    'y': 'years',
    't': 'minutes',
}


def extract_interval(interval):
    if re.match('^[1-9]+[DdWwMmYyTt]$', interval):
        interval_num = int(interval[:len(interval) - 1])
        interval_type = interval[len(interval) - 1:].lower()
        return interval_num, INTERVAL_DURATION[interval_type]

    raise ValueError


def display_interval(interval):
    try:
        interval_num, interval_type = extract_interval(interval)
    except ValueError:
        return None

    if interval_type == 'days':
        if interval_num == 1:
            return _('Daily')
        else:
            return _('Every {0} days'.format(interval_num))

    elif interval_type == 'weeks':
        if interval_num == 1:
            return _('Weekly')
        elif interval_num == 2:
            return _('Biweekly')
        else:
            return _('Every {0} weeks'.format(interval_num))

    elif interval_type == 'months':
        if interval_num == 1:
            return _('Monthly')
        elif interval_num == 3:
            return _('Quarterly')
        elif interval_num == 6:
            return _('Biannually')
        else:
            return _('Every {0} months'.format(interval_num))

    elif interval_type == 'years':
        if interval_num == 1:
            return _('Annually')
        else:
            return _('Every {0} years'.format(interval_num))

    elif interval_type == 'minutes':
        if interval_num == 1:
            return _('Every minutes')
        else:
            return _('Every {0} minutes'.format(interval_num))

    return None

