# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class GenericModel(models.Model):
    child = models.CharField(max_length=30)

    _cache = None

    def __unicode__(self):
        return '{} ({})'.format(self.child, self.id)

    class Meta:
        abstract = True

    @property
    def content_object(self):
        if self._cache:
            return self._cache
        else:
            return getattr(self, self.child, None)

    def save(self, *args, **kwargs):
        super(GenericModel, self).save(*args, **kwargs)

        if self.content_object:
            self.content_object.save()


class CommonTrashManager(models.Manager):
    def get_queryset(self):
        return super(CommonTrashManager, self).get_queryset().exclude(is_deleted=True)


class AbstractCommonTrashModel(models.Model):
    is_deleted = models.BooleanField('Is Deleted', default=False)
    objects = CommonTrashManager()

    class Meta:
        abstract = True

    def trash(self, *args, **kwargs):
        self.is_deleted = True
        self.save()
        return self

    def delete(self, *args, **kwargs):
        return self.trash(self, *args, **kwargs)

    def remove(self, *args, **kwargs):
        return super(AbstractCommonTrashModel, self).delete(*args, **kwargs)
