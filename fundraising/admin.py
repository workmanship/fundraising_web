from django.contrib import admin

from .models import Person, StoreTransaction


admin.site.register(Person)
admin.site.register(StoreTransaction)
