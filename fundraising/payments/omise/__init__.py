# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig

import omise

from fundraising import register_provider

from .app_settings import OMISE_SECRET_KEY, OMISE_PUBLIC_KEY


class OmisePaymentConfig(AppConfig):
    name = 'fundraising.payments.omise'
    verbose_name = "Omise Payment Provider"

    def ready(self):
        from .provider import OmisePaymentProvider
        register_provider(OmisePaymentProvider())


default_app_config = 'fundraising.payments.omise.OmisePaymentConfig'

omise.api_secret = OMISE_SECRET_KEY
omise.api_public = OMISE_PUBLIC_KEY
