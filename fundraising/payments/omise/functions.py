# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

import omise
from omise.errors import NotFoundError, UsedTokenError, FailedCaptureError, FailedRefundError, InvalidRecipientError, \
    BaseError

from fundraising.enums import TransactionActionStatus
from fundraising.payments.exceptions import PaymentException
from fundraising.payments.omise.exceptions import OMISE_ERROR_CODES
from fundraising.payments.omise.models import OmiseCustomer, OmiseCustomerCard, OmiseCard, OmiseTransaction, \
    OmiseTransferTransaction
from fundraising.payments.omise.provider import OmisePaymentProvider






def authorize_card(self, chargeable, omise_customer_card=None):
    # If card is not specified, use default
    omise_customer_card = self._get_default_card(chargeable.store_user, omise_customer_card)

    omise_transaction = OmiseTransaction.objects.create(
        chargeable=chargeable,
        amount=chargeable.charge_amount,
        action='authorize',
        payment_method=self.payment_method,
        payment_provider=self.provider_code,
        currency=chargeable.currency,
        customer_card=omise_customer_card,
    )

    request_data = omise_transaction.build_request(chargeable)
    omise_transaction.request_data = json.dumps(request_data)

    try:
        charge = omise.Charge.create(**request_data)

    except NotFoundError:
        omise_transaction.action_status = TransactionActionStatus.FAILED
        omise_transaction.fail_code = 'NotFoundError'

    else:
        omise_transaction.omise_transaction_id = charge.id
        omise_transaction.response_data = json.dumps(charge.__dict__.get('_attributes'))

        if charge.status == 'failed':
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = charge.failure_code

        elif charge.status == 'pending':
            omise_transaction.action_status = TransactionActionStatus.SUCCESS

        else:
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = charge.status

    omise_transaction.save()
    return omise_transaction


def capture_charge(self, authorized_transaction):
    if authorized_transaction.action_status != TransactionActionStatus.SUCCESS \
            or authorized_transaction.action != 'authorize':
        raise PaymentException('InvalidTransaction', OMISE_ERROR_CODES['InvalidTransaction'])

    chargeable = authorized_transaction.chargeable
    omise_transaction = OmiseTransaction.objects.create(
        chargeable=chargeable,
        amount=chargeable.charge_amount,
        action='capture',
        payment_method=self.payment_method,
        payment_provider=self.provider_code,
        currency=chargeable.currency,
        customer_card=authorized_transaction.customer_card,
        omise_transaction_id=authorized_transaction.omise_transaction_id
    )

    try:
        charge = omise.Charge.retrieve(omise_transaction.omise_transaction_id)
        charge.capture()

    except NotFoundError:
        omise_transaction.action_status = TransactionActionStatus.FAILED
        omise_transaction.fail_code = 'NotFoundError'

    except FailedCaptureError:
        omise_transaction.action_status = TransactionActionStatus.FAILED
        omise_transaction.fail_code = 'FailedCaptureError'

    else:
        if charge.status == 'failed':
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = charge.failure_code

        elif charge.status == 'successful':
            omise_transaction.action_status = TransactionActionStatus.SUCCESS

        else:
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = charge.status

    omise_transaction.save()
    return omise_transaction


def refund_transaction(self, charged_transaction, refund_amount=None):
    """
    The following requirements must be met for a charge to be refundable:
    - It must not be disputed
    - It must not have been fully refunded
    - It must not exceed the refund limit (15 refunds/charge)
    - It must be captured
    - It must have been captured for no longer than 1 year

    More Detail: https://www.omise.co/refunding-charges
    """

    if charged_transaction.action not in ('charge', 'capture'):
        raise PaymentException('InvalidTransaction', OMISE_ERROR_CODES['InvalidTransaction'])

    if not refund_amount:
        refund_amount = charged_transaction.amount

    chargeable = charged_transaction.chargeable
    omise_transaction = OmiseTransaction.objects.create(
        chargeable=chargeable,
        amount=refund_amount,
        action='refund',
        payment_method=self.payment_method,
        payment_provider=self.provider_code,
        currency=chargeable.currency,
        customer_card=charged_transaction.customer_card,
    )

    try:
        charge = omise.Charge.retrieve(charged_transaction.omise_transaction_id)

        request_data = omise_transaction.build_request(chargeable)
        omise_transaction.request_data = json.dumps(request_data)

        refund = charge.refund(**request_data)
        omise_transaction.response_data = json.dumps(charge.__dict__.get('_attributes'))

    except NotFoundError:
        omise_transaction.action_status = TransactionActionStatus.FAILED
        omise_transaction.fail_code = 'NotFoundError'

    except FailedRefundError:
        omise_transaction.action_status = TransactionActionStatus.FAILED
        omise_transaction.fail_code = 'FailedRefundError'

    else:
        omise_transaction.action_status = TransactionActionStatus.SUCCESS
        omise_transaction.omise_transaction_id = refund.id

    omise_transaction.save()
    return omise_transaction

def transfer_fund(self, amount, recipient=None):
    # There must be over 30 THB in your available balance.

    # TODO : Check current balance

    omise_transaction = OmiseTransferTransaction.objects.create(
        amount=amount,
        omise_recipient_id=recipient
    )

    request_data = omise_transaction.build_request()
    omise_transaction.request_data = json.dumps(request_data)

    try:
        transfer = omise.Transfer.create(**request_data)

        omise_transaction.omise_transaction_id = transfer.id
        omise_transaction.response_data = json.dumps(transfer.__dict__.get('_attributes'))

    except InvalidRecipientError:
        omise_transaction.action_status = OmiseTransaction.FAILED
        omise_transaction.fail_code = 'InvalidRecipientError'

    except BaseError:
        omise_transaction.action_status = OmiseTransaction.FAILED
        omise_transaction.fail_code = 'InvalidAmount'

    else:
        omise_transaction.action_status = OmiseTransaction.SUCCESS
        omise_transaction.omise_transaction_id = transfer.id

    omise_transaction.save()
    return omise_transaction