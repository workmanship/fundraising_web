# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from fundraising.models import StoreTransaction, AbstractTransaction
from fundraising.payments.omise.exceptions import OMISE_ERROR_CODES, UNKNOWN_ERROR_MESSAGE
from fundraising.utils.decimal import decimal_to_satang


# Omise Transaction
# ----------------------------------------------------------------------------------------------------------------------

class OmiseTransaction(AbstractTransaction):
    transaction = models.OneToOneField('fundraising.StoreTransaction', related_name='omise_transaction')

    omise_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    customer_card = models.ForeignKey('OmiseCustomerCard', related_name='omise_transactions')

    def build_request(self, chargeable):
        if self.action == 'charge':
            return {
                'amount': decimal_to_satang(chargeable.charge_amount),
                'currency': chargeable.currency,
                'description': str(chargeable),
                'card': self.customer_card.card.card_id,
                'customer': self.customer_card.customer.omise_customer_id,
                'capture': True
            }
        elif self.action == 'authorize':
            return {
                'amount': decimal_to_satang(chargeable.charge_amount),
                'currency': chargeable.currency,
                'description': str(chargeable),
                'card': self.customer_card.card.card_id,
                'customer': self.customer_card.customer.omise_customer_id,
                'capture': False
            }
        elif self.action == 'refund':
            return {
                'amount': decimal_to_satang(chargeable.charge_amount),
                'currency': chargeable.currency,
            }

        return {}

    @property
    def is_failed_permanently(self):
        if self.fail_code in ('invalid_card',):
            return True
        return False

    @property
    def fail_message(self):
        error_message = OMISE_ERROR_CODES.get(self.fail_code)
        return error_message if error_message else UNKNOWN_ERROR_MESSAGE


# Omise Recurring
# ----------------------------------------------------------------------------------------------------------------------

class OmiseRecurringInstruction(models.Model):
    instruction = models.OneToOneField('fundraising.StoreRecurringInstruction', related_name='omise')
    customer_card = models.ForeignKey('OmiseCustomerCard', null=True, blank=True)


# Omise Card
# ----------------------------------------------------------------------------------------------------------------------

class OmiseCard(models.Model):
    card_id = models.CharField(max_length=50)
    card_name = models.CharField(max_length=100, null=True, blank=True)
    card_city = models.CharField(max_length=100, null=True, blank=True)
    card_country = models.CharField(max_length=5, null=True, blank=True)
    card_postal_code = models.CharField(max_length=20, null=True, blank=True)
    card_last_digits = models.CharField(max_length=4)
    card_brand = models.CharField(max_length=20)
    card_bank = models.CharField(max_length=100)
    card_financing = models.CharField(max_length=100, null=True, blank=True)
    card_expiration_month = models.PositiveSmallIntegerField(null=True, blank=True)
    card_expiration_year = models.PositiveSmallIntegerField(null=True, blank=True)

    def get_card_last_digits_display(self):
        if self.card_last_digits:
            return '**** **** **** {last_digits}'.format(last_digits=self.card_last_digits)

    def card_expiration_month_year(self):
        if self.card_expiration_month and self.card_expiration_year:
            return '{month:02d} / {year}'.format(month=self.card_expiration_month,
                                                 year=str(self.card_expiration_year)[-2:])
        return ''

    """
    def get_card_expiration_date_display(self):
        if self.card_expiration_month and self.card_expiration_year:
            return '{month} / {year}'.format(month=self.card_expiration_month, year=self.card_expiration_year)
    """


# Omise Customer
# ----------------------------------------------------------------------------------------------------------------------

class OmiseCustomer(models.Model):
    person = models.OneToOneField('fundraising.Person', related_name='omise_customer')
    omise_customer_id = models.CharField(max_length=50)
    default_card = models.ForeignKey('OmiseCustomerCard', null=True, blank=True)


class OmiseCustomerCardQuerySet(models.QuerySet):
    def is_active(self):
        return self.filter(is_destroyed=False)


class OmiseCustomerCardManager(models.Manager):
    def get_queryset(self):
        return OmiseCustomerCardQuerySet(self.model, using=self._db)

    def is_active(self):
        return self.get_queryset().is_active()


class OmiseCustomerCard(models.Model):
    customer = models.ForeignKey('OmiseCustomer', related_name='cards')
    card = models.ForeignKey('OmiseCard', related_name='customers')
    is_destroyed = models.BooleanField(default=False)

    objects = OmiseCustomerCardManager()


# Transfer to Bank Account
# ----------------------------------------------------------------------------------------------------------------------

class OmiseTransferTransaction(AbstractTransaction):
    transaction = models.OneToOneField('fundraising.StoreTransaction', related_name='omise_transfer_transaction')

    omise_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    omise_recipient_id = models.CharField(max_length=50, null=True, blank=True)

    def build_request(self):
        return {
            'amount': decimal_to_satang(self.amount),
            'currency': self.currency,
            'recipient': self.omise_recipient_id
        }
