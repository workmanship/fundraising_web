# -*- coding: utf-8 -*-
from __future__ import unicode_literals

UNKNOWN_ERROR_MESSAGE = 'Unknown payment error'


OMISE_ERROR_CODES = {
    'NotFoundError': 'Necessary payment information is not found on our server',
    'UsedTokenError': 'This token is already been used.',
    'FailedCaptureError': '',

    'InvalidTransaction': 'Transaction is invalid',
    'NonRefundable': 'This transaction is non-refundable',
    'FailedRefundError': '',

    'InvalidAmount': '',
    'InvalidRecipientError': '',

    'insufficient_fund': "Insufficient funds in the account or the card has reached the credit limit.",
    'stolen_or_lost_card': "Card is stolen or lost.",
    'failed_processing': "Card processing has failed, you may retry this transaction.",
    'payment_rejected': "The payment was rejected by the issuer or the acquirer with no specific reason.",
    'invalid_security_code': "The security code was invalid or the card didn't pass preauth.",
    'failed_fraud_check': "Card was marked as fraudulent.",
    'invalid_account_number': "This number is not attributed or has been deactivated.",
}
