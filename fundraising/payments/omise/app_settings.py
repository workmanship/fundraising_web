"""
Settings for Omise Payment
"""

from django.conf import settings

OMISE_SECRET_KEY = getattr(settings, 'OMISE_SECRET_KEY', '')
OMISE_PUBLIC_KEY = getattr(settings, 'OMISE_PUBLIC_KEY', '')

RECURRING_FUND_TRANSFER_DATE = getattr(settings, 'OMISE_RECURRING_FUND_TRANSFER_DATE', 1)
MONTHS_TO_REMIND_CARD_EXPIRED = getattr(settings, 'OMISE_MONTHS_TO_REMIND_CARD_EXPIRED', 2)
