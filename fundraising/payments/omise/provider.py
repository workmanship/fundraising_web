# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from datetime import date
from dateutil.relativedelta import relativedelta

from django.utils.translation import ugettext_lazy as _

import omise
from omise.errors import NotFoundError, UsedTokenError

from fundraising.enums import TransactionActionStatus
from fundraising.models import StoreRecurringSequenceTransaction
from fundraising.payments.exceptions import PaymentException
from fundraising.payments.omise.exceptions import OMISE_ERROR_CODES
from fundraising.payments.providers import PaymentProvider

from . import app_settings
from .models import OmiseCustomer, OmiseTransaction, OmiseCustomerCard, OmiseCard, OmiseRecurringInstruction


class OmisePaymentProvider(PaymentProvider):
    CREDIT_CARD_METHOD = 'creditcard'
    INTERNET_BANKING_METHOD = 'ibanking'

    provider_code = 'omise'
    payment_methods = (CREDIT_CARD_METHOD, INTERNET_BANKING_METHOD)

    @staticmethod
    def get_provider_code():  # todo : remove?
        return OmisePaymentProvider.provider_code

    @staticmethod
    def get_payment_method_name(payment_method):
        if payment_method == OmisePaymentProvider.CREDIT_CARD_METHOD:
            return _('Omise / Credit Card')
        elif payment_method == OmisePaymentProvider.INTERNET_BANKING_METHOD:
            return _('Omise / Internet Banking')
        return ''

    @staticmethod
    def get_context_settings():
        return {'OMISE_PUBLIC_KEY': app_settings.OMISE_PUBLIC_KEY}

    # Private functions
    # ------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def _get_default_card(person, omise_customer_card):
        if not omise_customer_card:
            try:
                omise_customer = OmiseCustomer.objects.get(person=person)
            except OmiseCustomer.DoesNotExist:
                raise PaymentException('create_customer_first', '')

            if omise_customer.default_card:
                omise_customer_card = omise_customer.default_card
            else:
                omise_customer_card = OmiseCustomerCard.objects.filter(customer=omise_customer).first()

                if omise_customer_card:
                    omise_customer.default_card = omise_customer_card
                    omise_customer.save()
                else:
                    raise PaymentException('customer_has_no_card', '')

        return omise_customer_card

    # Customer
    # ------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def create_customer_with_card(person, omise_token, omise_card_id):
        try:
            # Check if we already create an Omise customer
            omise_customer = OmiseCustomer.objects.get(person=person)

            try:
                # Check if our OmiseCustomer record is available in Omise server
                customer = omise.Customer.retrieve(omise_customer.omise_customer_id)

            except NotFoundError:
                # Omise doesn't have our OmiseCustomer, so we create a new one
                customer = omise.Customer.create(
                    email=person.email,
                    description="{email} (id: {id})".format(email=person.email, id=person.id),
                )

                omise_customer.omise_customer_id = customer.id
                omise_customer.default_card = None  # Remove related card
                omise_customer.save()

                # Destroy all related cards
                OmiseCustomerCard.objects.filter(customer=omise_customer).update(is_destroyed=True)

        except OmiseCustomer.DoesNotExist:
            customer = omise.Customer.create(
                email=person.email,
                description="{email} (id: {id})".format(email=person.email, id=person.id),
            )

            omise_customer = OmiseCustomer.objects.create(person=person, omise_customer_id=customer.id)

        # Add card to customer
        try:
            customer = customer.update(card=omise_token)
        except UsedTokenError:
            raise PaymentException('UsedTokenError', OMISE_ERROR_CODES['UsedTokenError'])

        try:
            card = customer.cards.retrieve(omise_card_id)
        except NotFoundError:
            raise PaymentException('NotFoundError', OMISE_ERROR_CODES['NotFoundError'])

        if card:
            omise_card = OmiseCard.objects.create(
                card_id=card.id,
                card_name=card.name,
                card_city=card.city,
                card_country=card.country,
                card_postal_code=card.postal_code,
                card_last_digits=card.last_digits,
                card_brand=card.brand,
                card_bank=card.bank,
                card_financing=card.financing,
                card_expiration_month=card.expiration_month,
                card_expiration_year=card.expiration_year
            )

            omise_customer_card = OmiseCustomerCard.objects.create(customer=omise_customer, card=omise_card)
        else:
            raise PaymentException('NotFoundError', OMISE_ERROR_CODES['NotFoundError'])

        if not omise_customer.default_card:
            omise_customer.default_card = omise_customer_card
            omise_customer.save()

        return omise_customer_card

    def add_card_to_customer(self, omise_customer, omise_token):
        # TODO
        pass

    def destroy_card(self):
        # TODO
        pass

    # Charge
    # ------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def charge_card(chargeable, omise_customer_card=None):
        # If card is not specified, use default
        omise_customer_card = OmisePaymentProvider._get_default_card(chargeable.person, omise_customer_card)

        omise_transaction = OmiseTransaction.objects.create(
            chargeable=chargeable,
            amount=chargeable.charge_amount,
            action='charge',
            payment_method=OmisePaymentProvider.provider_code,
            payment_provider=OmisePaymentProvider.CREDIT_CARD_METHOD,
            currency=chargeable.currency,
            customer_card=omise_customer_card,
        )

        request_data = omise_transaction.build_request(chargeable)
        omise_transaction.request_data = json.dumps(request_data)

        try:
            charge = omise.Charge.create(**request_data)

        except NotFoundError:
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = 'NotFoundError'

        else:
            omise_transaction.omise_transaction_id = charge.id
            omise_transaction.response_data = json.dumps(charge.__dict__.get('_attributes'))

            if charge.status == 'failed':
                omise_transaction.action_status = TransactionActionStatus.FAILED
                omise_transaction.fail_code = charge.failure_code

                # TODO : Add fail_message

            elif charge.status == 'successful':
                omise_transaction.action_status = TransactionActionStatus.SUCCESS

            else:
                omise_transaction.action_status = TransactionActionStatus.FAILED
                omise_transaction.fail_code = charge.status

        omise_transaction.save()
        return omise_transaction

    def refund_transaction(self, charged_transaction, refund_amount=None):
        # TODO
        pass

    # Recurring
    # ------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def initialize_recurring(instruction, omise_customer_card=None):
        OmiseRecurringInstruction.objects.create(instruction=instruction.instruction, customer_card=omise_customer_card)

    @staticmethod
    def charge_recurring_sequence(sequence):

        # TODO : Check if fail is permanent, add 'is_failed_permanently'

        chargeable = sequence.instruction.chargeable

        omise_instruction = OmiseRecurringInstruction.objects.get(instruction=sequence.store_recurring_instruction)

        omise_transaction = OmiseTransaction.objects.create(
            chargeable=chargeable,
            amount=chargeable.charge_amount,
            action='charge',
            payment_method=OmisePaymentProvider.CREDIT_CARD_METHOD,
            payment_provider=OmisePaymentProvider.provider_code,
            action_status=TransactionActionStatus.PROCESSING,
            currency=chargeable.currency,
            customer_card=omise_instruction.customer_card
        )

        StoreRecurringSequenceTransaction.objects.create(
            sequence=sequence.to_store_recurring_sequence(),
            transaction=omise_transaction.to_store_transaction()
        )

        request_data = omise_transaction.build_request(chargeable)
        omise_transaction.request_data = json.dumps(request_data)

        try:
            charge = omise.Charge.create(**request_data)
        except NotFoundError:
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = 'NotFoundError'
        except:
            omise_transaction.action_status = TransactionActionStatus.FAILED
            omise_transaction.fail_code = 'error'

        else:
            omise_transaction.omise_transaction_id = charge.id
            omise_transaction.response_data = json.dumps(charge.__dict__.get('_attributes'))

            if charge.status == 'failed':
                omise_transaction.action_status = TransactionActionStatus.FAILED
                omise_transaction.fail_code = charge.failure_code

            elif charge.status == 'successful':
                omise_transaction.action_status = TransactionActionStatus.SUCCESS

            else:
                omise_transaction.action_status = TransactionActionStatus.FAILED
                omise_transaction.fail_code = charge.status

        omise_transaction.save()

        return omise_transaction

    @staticmethod
    def check_card_expiration_for_recurring(instruction):
        store_recurring_instruction = instruction.to_store_recurring_instruction()

        omise_instruction = OmiseRecurringInstruction.objects.get(instruction=store_recurring_instruction)
        omise_card = omise_instruction.customer_card.card

        start_of_month = date.today().replace(day=1)
        expire_date = date(omise_card.card_expiration_year, omise_card.card_expiration_month, 1)

        delta = relativedelta(expire_date, start_of_month)
        return delta.months + (delta.years * 12)  # TODO ???
