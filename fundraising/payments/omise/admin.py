from django.contrib import admin

from .models import OmiseTransaction, OmiseCard, OmiseCustomer, OmiseCustomerCard

admin.site.register(OmiseTransaction)
admin.site.register(OmiseCard)
admin.site.register(OmiseCustomer)
admin.site.register(OmiseCustomerCard)