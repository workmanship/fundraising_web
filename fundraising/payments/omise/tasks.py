# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

from django.db.models import Sum
from dateutil.relativedelta import relativedelta

from website.celery import app
from .app_settings import RECURRING_FUND_TRANSFER_DATE, MONTHS_TO_REMIND_CARD_EXPIRED
from .models import StoreTransaction
from fundraising.modules.fund_donation import signals
from fundraising.modules.fund_donation.models import DonationRecurringInstruction
from fundraising.payments.providers import get_payment_provider


@app.task(bind=True)
def transfer_fund_monthly(self):
    end = date.today().replace(day=RECURRING_FUND_TRANSFER_DATE)
    start = end - relativedelta(months=1)

    monthly_amount = StoreTransaction.objects.filter(
        action__in=['charge', 'capture'],
        action_status=StoreTransaction.SUCCESS,
        modified__range=[start, end]
    ).aggregate(Sum('amount'))['amount__sum']

    provider = get_payment_provider('omise')
    provider.transfer_fund(amount=monthly_amount)


@app.task(bind=True)
def remind_card_expiration(self):
    for instruction in DonationRecurringInstruction.objects.filter(is_active=True, payment_method='creditcard'):
        provider = get_payment_provider(instruction.chargeable.payment_provider)
        expired_in_months = provider.check_card_expiration_for_recurring(instruction)

        if expired_in_months == 0:
            signals.credit_card_expired.send(sender=remind_card_expiration, donation=instruction.donation)

        elif expired_in_months <= MONTHS_TO_REMIND_CARD_EXPIRED:
            signals.credit_card_almost_expired.send(sender=remind_card_expiration, donation=instruction.donation)
