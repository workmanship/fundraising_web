
class PaymentException(Exception):
    error_code = ''

    def __init__(self, error_code, message, *args, **kwargs):
        self.error_code = error_code
        super(PaymentException, self).__init__(message, *args, **kwargs)
