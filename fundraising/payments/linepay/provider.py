# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from fundraising.payments.providers import PaymentProvider


class LinePayPaymentProvider(PaymentProvider):
    provider_code = 'linepay'
    payment_methods = ('linepay', )

    def get_provider_code(self):
        return self.provider_code

    def get_payment_method_name(self, payment_method):
        return _('Rabbit LINE Pay')

    def get_context_settings(self):
        return {}
