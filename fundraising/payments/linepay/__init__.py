# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig

from fundraising import register_provider


class LinePayPaymentConfig(AppConfig):
    name = 'fundraising.payments.linepay'
    verbose_name = "LINE Pay Payment Provider"

    def ready(self):
        from .provider import LinePayPaymentProvider
        register_provider(LinePayPaymentProvider())


default_app_config = 'fundraising.payments.linepay.LinePayPaymentConfig'
