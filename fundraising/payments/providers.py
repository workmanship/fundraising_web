# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .. import payment_providers, valid_payment_options


class PaymentProvider(object):
    """
    Base class for Payment Service Providers.
    """

    @staticmethod
    def get_provider_code():
        raise NotImplementedError

    @staticmethod
    def get_payment_method_name(payment_method):
        raise NotImplementedError

    @staticmethod
    def get_context_settings():
        return {}

    @staticmethod
    def initialize_recurring(instruction):
        raise NotImplementedError

    @staticmethod
    def charge_recurring_sequence(sequence):
        raise NotImplementedError

    def refund_transaction(self, charged_transaction, refund_amount=None):
        raise NotImplementedError()

    """
    def check_card_expiration_for_recurring(self, instruction):
        raise NotImplementedError()
    """


def get_payment_provider(provider_code):
    try:
        return payment_providers[provider_code]
    except KeyError:
        raise KeyError('Payment provider "%s" is not registered.' % provider_code)


def is_valid_payment_option(payment_option):
    return payment_option in valid_payment_options


def extract_payment_option(payment_option):
    try:
        provider_code, payment_method = payment_option.split(':')
    except ValueError:
        provider_code = payment_option
        payment_method = ''

    return provider_code, payment_method
